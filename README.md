# README #
This is git repository for PA165 course project at Masaryk University. 

### Assignment ###

Goal of this project is to develop a mushroom hunter portal where mushroom hunters can exchange information about locations rich for mushrooms and also locations that are poor for mushrooms. The mushroom hunter have at least these attributes: firstname, surname, optional personal information (some text description that they choose for themselves). A forest is another important entity in the system. It has a name and description of its locality. A visit of the forest is a relationship between mushroom hunter and a forest and is described by a date of visit and note (e.g. "it was very dry, no mushrooms found"). A mushroom has a name, type (edible, unedible, poisonous). and interval of occurence in the following string format: "June - July" (month - month). THe portal should contain a catalogue of mushrooms. The catalogue should be searchable, filterable and sortable. The user of the portal can also list visits of a forest (default sorting should be the most recent visit first) with a possibility of sorting by forest or mushroom hunter. The user of the portal will be also able to perform CRUD operations on the following: mushrooms, forests, mushroom hunters, visits.

## How do I get set up? ##

[Follow wiki pages](https://bitbucket.org/jirkajansky/pa165_project/wiki/Home)

### How do I log in? ###
There's default administrator account:

Login: admin@admin.cz

Password: 1234

*Default values can be changed in property file.*

### How to run Web module: ###
#### 1. via Netbeans ####
Right-click on MushroomHunterPortalWeb project -> Custom -> Goals... -> Goals: spring-boot:run and OK. You can also save goal for later use if you check Remember as: and write shortcut name.

#### 2. via Maven ####
Go to MushroomHunterPortalWeb folder and write

```
#!java

mvn spring-boot:run
```
### How to run REST client: ###

#### 1. From Netbrans ####
Run as standard desktop application with class 
```
#!java

cz.muni.fi.pa165.restclient.core.Main
```
as main class.

#### 2. From maven ####
Run 

```
#!java

mvn exec:java -Dexec.mainClass="cz.muni.fi.pa165.restclient.core.Main"
```
in command line.

### How to configure local derby database:
In netbeans go to Services window -> Databasees -> JavaDB - right click + Create database with following properties:

* Database name: pa165
* User name:  pa165
* Password: pa165

### Who do I talk to? ###

* **Project lead**:
Jan Bruzl
359578@mail.muni.cz

### Participants ###

* @AdiC : 359578
* @jirkajansky : 436331
* @trnkajn : 430624

#### Inactive ####
* @martinaproch : 430573