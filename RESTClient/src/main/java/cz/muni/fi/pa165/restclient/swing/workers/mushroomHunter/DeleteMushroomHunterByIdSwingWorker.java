/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.restclient.swing.workers.mushroomHunter;

import javax.swing.SwingWorker;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Jan Bruzl
 */
public class DeleteMushroomHunterByIdSwingWorker extends SwingWorker<Object, Object> {
    private final String restCode = "/rest/hunters/delete";
    private String url;
    private long id;

    public DeleteMushroomHunterByIdSwingWorker(String url, long id) {
        this.url = url;
        this.id = id;
    }

    @Override
    protected Object doInBackground() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String restUrl = url + restCode;
        restTemplate.delete(restUrl+"?id={id}", id);
        return null;
    }

}
