/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.restclient.swing.workers.mushroomHunter;

import cz.muni.fi.pa165.restclient.entities.MushroomHunter;
import javax.swing.SwingWorker;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Jan Bruzl
 */
public class AddMushroomHunterSwingWorker extends SwingWorker<Class<Void>, Object> {

    private final String restCode = "/rest/hunters/add";
    private String url;

    private MushroomHunter mushroomHunter;

    public AddMushroomHunterSwingWorker(String url, MushroomHunter mushroomHunter) {
        this.url = url;
        this.mushroomHunter = mushroomHunter;
    }

    @Override
    protected Class<Void> doInBackground() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String restUrl = url + restCode;
        restTemplate.postForObject(restUrl, mushroomHunter, String.class);
        
        return Void.TYPE;
    }

}
