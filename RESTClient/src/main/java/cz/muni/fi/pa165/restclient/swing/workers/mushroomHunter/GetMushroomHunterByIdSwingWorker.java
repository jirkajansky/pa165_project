/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.restclient.swing.workers.mushroomHunter;

import cz.muni.fi.pa165.restclient.entities.MushroomHunter;
import javax.swing.SwingWorker;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Jan Bruzl
 */
public class GetMushroomHunterByIdSwingWorker extends SwingWorker<MushroomHunter, Object> {
    private final String restCode = "/rest/hunters/detail";
    private String url;
    
    private long id;

    public GetMushroomHunterByIdSwingWorker(String url, long id) {
        this.url = url;
        this.id = id;
    }

    @Override
    protected MushroomHunter doInBackground() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String restUrl = url + restCode;
        MushroomHunter m = restTemplate.getForObject(restUrl+"?id={id}", MushroomHunter.class, id);
        return m;
    }

}
