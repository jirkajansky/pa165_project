/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.restclient.swing.workers.mushroom;

import cz.muni.fi.pa165.restclient.entities.Mushroom;
import javax.swing.SwingWorker;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Jan Bruzl
 */
public class GetMushroomByIdSwingWorker extends SwingWorker<Mushroom, Object> {
    private final String restCode = "/rest/mushrooms/detail";
    private String url;
    
    private long id;

    public GetMushroomByIdSwingWorker(String url, long id) {
        this.url = url;
        this.id = id;
    }

    @Override
    protected Mushroom doInBackground() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String restUrl = url + restCode;
        Mushroom m = restTemplate.getForObject(restUrl+"?id={id}", Mushroom.class, id);
        return m;
    }

}
