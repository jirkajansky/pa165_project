/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.restclient.swing.workers.mushroom;

import cz.muni.fi.pa165.restclient.entities.Mushroom;
import javax.swing.SwingWorker;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Jan Bruzl
 */
public class UpdateMushroomSwingWorker extends SwingWorker<Class<Void>, Object>{
    private String url;
    private final String restCode = "/rest/mushrooms/edit";
    private Mushroom mushroom;

    public UpdateMushroomSwingWorker(String url, Mushroom mushroom) {
        this.url = url;
        this.mushroom = mushroom;
    }
    
    
    @Override
    protected Class<Void> doInBackground() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String restUrl = url + restCode;
        restTemplate.postForObject(restUrl, mushroom, String.class);
        return Void.TYPE;
    }
    
}
