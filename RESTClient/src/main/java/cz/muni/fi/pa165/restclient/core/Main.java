/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.restclient.core;

import cz.muni.fi.pa165.restclient.swing.MainFrame;
import javax.swing.JFrame;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Main class
 * @author Jan Bruzl
 */
@Configuration
@ComponentScan(basePackages = "cz.muni.fi.pa165.restclient")
@EnableAutoConfiguration
public class Main extends JFrame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("mac")) { // if mac
            System.setProperty("apple.laf.useScreenMenuBar", "true");
        }
        ApplicationContext context = SpringApplication.run(Main.class, args);
        MainFrame mainFrame = context.getBean(MainFrame.class);
        mainFrame.run();
    }

}
