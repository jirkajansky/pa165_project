package cz.muni.fi.pa165.restclient.entities;

/**
 * Mushroom type enum
 * @author Jan Bruzl
 */
public enum MushroomType {
    EDIBLE, UNEDIBLE, POISONOUS
}
