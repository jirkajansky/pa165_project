/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.restclient.swing;

import cz.muni.fi.pa165.restclient.swing.workers.mushroom.DeleteMushroomByIdSwingWorker;
import cz.muni.fi.pa165.restclient.entities.Mushroom;
import cz.muni.fi.pa165.restclient.entities.MushroomHunter;
import cz.muni.fi.pa165.restclient.entities.MushroomType;
import cz.muni.fi.pa165.restclient.swing.workers.mushroom.AddMushroomSwingWorker;
import cz.muni.fi.pa165.restclient.swing.workers.mushroom.GetAllMushroomsSwingWorker;
import cz.muni.fi.pa165.restclient.swing.workers.mushroom.GetMushroomByIdSwingWorker;
import cz.muni.fi.pa165.restclient.swing.workers.mushroom.GetMushroomsByTypeSwingWorker;
import cz.muni.fi.pa165.restclient.swing.workers.mushroom.UpdateMushroomSwingWorker;
import cz.muni.fi.pa165.restclient.swing.workers.mushroomHunter.AddMushroomHunterSwingWorker;
import cz.muni.fi.pa165.restclient.swing.workers.mushroomHunter.DeleteMushroomHunterByIdSwingWorker;
import cz.muni.fi.pa165.restclient.swing.workers.mushroomHunter.GetAllMushroomsHunterSwingWorker;
import cz.muni.fi.pa165.restclient.swing.workers.mushroomHunter.GetMushroomHunterByIdSwingWorker;
import cz.muni.fi.pa165.restclient.swing.workers.mushroomHunter.UpdateMushroomHunterSwingWorker;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Jan Bruzl
 */
@Component
public class MainFrame extends javax.swing.JFrame {

    @Value("${server.timeout}")
    private int workerTimeout;

    @Value("${server.url}")
    private String serverUrl;

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        OutputTextPane = new javax.swing.JTextPane();
        jLabel1 = new javax.swing.JLabel();
        MushroomPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        MushroomHunterPanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        OutputTextPane.setEditable(false);
        OutputTextPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        OutputTextPane.setFont(new java.awt.Font("Courier New", 0, 11)); // NOI18N
        jScrollPane1.setViewportView(OutputTextPane);

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default"); // NOI18N
        jLabel1.setText(bundle.getString("OUTPUT")); // NOI18N

        MushroomPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText(bundle.getString("MUSHROOM")); // NOI18N

        jButton1.setText(bundle.getString("GET ALL")); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText(bundle.getString("GET BY ID")); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText(bundle.getString("ADD NEW")); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText(bundle.getString("EDIT")); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText(bundle.getString("DELETE")); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton7.setText(bundle.getString("GET EDIBLE")); // NOI18N
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setText(bundle.getString("GET UNEDIBLE")); // NOI18N
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setText(bundle.getString("GET POISONOUS")); // NOI18N
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout MushroomPanelLayout = new javax.swing.GroupLayout(MushroomPanel);
        MushroomPanel.setLayout(MushroomPanelLayout);
        MushroomPanelLayout.setHorizontalGroup(
            MushroomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MushroomPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MushroomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(MushroomPanelLayout.createSequentialGroup()
                        .addGroup(MushroomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(MushroomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        MushroomPanelLayout.setVerticalGroup(
            MushroomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MushroomPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(MushroomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(MushroomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton8)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(MushroomPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton9)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton5)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        MushroomHunterPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setText(bundle.getString("MUSHROOM HUNTER")); // NOI18N

        jButton6.setText(bundle.getString("GET ALL")); // NOI18N
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton13.setText(bundle.getString("DELETE")); // NOI18N
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        jButton11.setText(bundle.getString("ADD NEW")); // NOI18N
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton12.setText(bundle.getString("EDIT")); // NOI18N
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jButton10.setText(bundle.getString("GET BY ID")); // NOI18N
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout MushroomHunterPanelLayout = new javax.swing.GroupLayout(MushroomHunterPanel);
        MushroomHunterPanel.setLayout(MushroomHunterPanelLayout);
        MushroomHunterPanelLayout.setHorizontalGroup(
            MushroomHunterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MushroomHunterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MushroomHunterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        MushroomHunterPanelLayout.setVerticalGroup(
            MushroomHunterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MushroomHunterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton13)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jMenu1.setText(bundle.getString("FILE")); // NOI18N

        jMenuItem1.setText(bundle.getString("CLOSE")); // NOI18N
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 501, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(MushroomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(MushroomHunterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jLabel1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(MushroomPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                        .addComponent(MushroomHunterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        dispose();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        GetAllMushroomsSwingWorker getAllMushroomsSwingWorker = new GetAllMushroomsSwingWorker(serverUrl);

        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING GETALL MUSHROOMS..."));
        getAllMushroomsSwingWorker.execute();
        try {
            OutputTextPane.setText(getAllMushroomsSwingWorker.get(workerTimeout, TimeUnit.MILLISECONDS).toString());
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        Long id = getIDByDialog();

        DeleteMushroomByIdSwingWorker dmbisw = new DeleteMushroomByIdSwingWorker(serverUrl, id);
        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING DELETE MUSHROOM..."));
        dmbisw.execute();
        try {
            dmbisw.get(workerTimeout, TimeUnit.MILLISECONDS);
            OutputTextPane.setText(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("DELETED MUSHROOM WITH ID = {0}"), new Object[]{id}));
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        MushroomType mushroomType = MushroomType.EDIBLE;
        getMushroomsByType(mushroomType);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        MushroomType mushroomType = MushroomType.UNEDIBLE;
        getMushroomsByType(mushroomType);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        MushroomType mushroomType = MushroomType.POISONOUS;
        getMushroomsByType(mushroomType);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Long id = getIDByDialog();

        GetMushroomByIdSwingWorker gmbisw = new GetMushroomByIdSwingWorker(serverUrl, id);
        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING GET MUSHROOM..."));
        gmbisw.execute();
        try {
            Mushroom mushroom = gmbisw.get(workerTimeout, TimeUnit.MILLISECONDS);
            if (mushroom == null) {
                OutputTextPane.setText(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("NOTHING FOUND WIRH ID: {0}"), new Object[]{id}));
            } else {
                OutputTextPane.setText(mushroom.toString());
            }

        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        Mushroom mushroom = getMushroomByDialog(new Mushroom());
        AddMushroomSwingWorker amsw = new AddMushroomSwingWorker(serverUrl, mushroom);
        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING ADD MUSHROOM..."));
        amsw.execute();
        try {
            amsw.get(workerTimeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        Mushroom mushroom = null;
        Long id = getIDByDialog();

        GetMushroomByIdSwingWorker gmbisw = new GetMushroomByIdSwingWorker(serverUrl, id);
        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING GET MUSHROOM..."));
        gmbisw.execute();
        try {
            mushroom = gmbisw.get(workerTimeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
            return;
        }

        if (mushroom == null) {
            OutputTextPane.setText(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("NOTHING FOUND WIRH ID: {0}"), new Object[]{id}));
            return;
        }

        mushroom = getMushroomByDialog(mushroom);

        UpdateMushroomSwingWorker amsw = new UpdateMushroomSwingWorker(serverUrl, mushroom);
        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING UPDATE MUSHROOM..."));
        amsw.execute();
        try {
            amsw.get(workerTimeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        GetAllMushroomsHunterSwingWorker getAllMushroomsHunterSwingWorker = new GetAllMushroomsHunterSwingWorker(serverUrl);

        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING GETALL MUSHROOM HUNTERS..."));
        getAllMushroomsHunterSwingWorker.execute();
        try {
            OutputTextPane.setText(getAllMushroomsHunterSwingWorker.get(workerTimeout, TimeUnit.MILLISECONDS).toString());
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        Long id = getIDByDialog();

        GetMushroomHunterByIdSwingWorker gmbisw = new GetMushroomHunterByIdSwingWorker(serverUrl, id);
        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING GET MUSHROOM HUNTER..."));
        gmbisw.execute();
        try {
            MushroomHunter mushroomHunter = gmbisw.get(workerTimeout, TimeUnit.MILLISECONDS);
            if (mushroomHunter == null) {
                OutputTextPane.setText(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("NOTHING FOUND WIRH ID: {0}"), new Object[]{id}));
            } else {
                OutputTextPane.setText(mushroomHunter.toString());
            }

        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
        }
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        MushroomHunter mushroomHunter = getMushroomHunterByDialog(new MushroomHunter());

        AddMushroomHunterSwingWorker amsw = new AddMushroomHunterSwingWorker(serverUrl, mushroomHunter);
        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING ADD MUSHROOM HUNTER..."));
        amsw.execute();
        try {
            amsw.get(workerTimeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
        }
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        MushroomHunter mushroomHunter = null;
        Long id = getIDByDialog();

        GetMushroomHunterByIdSwingWorker gmbisw = new GetMushroomHunterByIdSwingWorker(serverUrl, id);
        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING GET MUSHROOM HUNTER..."));
        gmbisw.execute();
        try {
            mushroomHunter = gmbisw.get(workerTimeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
            return;
        }

        if (mushroomHunter == null) {
            OutputTextPane.setText("Nothing found wirh id: " + id);
            return;
        }

        mushroomHunter = getMushroomHunterByDialog(mushroomHunter);

        UpdateMushroomHunterSwingWorker amsw = new UpdateMushroomHunterSwingWorker(serverUrl, mushroomHunter);
        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING UPDATE MUSHROOM..."));
        amsw.execute();
        try {
            amsw.get(workerTimeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
        }
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        Long id = getIDByDialog();

        DeleteMushroomHunterByIdSwingWorker dmbisw = new DeleteMushroomHunterByIdSwingWorker(serverUrl, id);
        OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING DELETE MUSHROOM HUNTER..."));
        dmbisw.execute();
        try {
            dmbisw.get(workerTimeout, TimeUnit.MILLISECONDS);
            OutputTextPane.setText(java.text.MessageFormat.format(
                    java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("DELETED MUSHROOM WITH ID = {0}"),
                    new Object[]{id}
            ));
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
        }
    }//GEN-LAST:event_jButton13ActionPerformed

    protected Long getIDByDialog() {
        InsertIDDialog iDDialog = new InsertIDDialog(this, true);
        iDDialog.pack();
        iDDialog.setVisible(true);
        return iDDialog.getID();
    }

    protected Mushroom getMushroomByDialog(Mushroom mushroom) {
        MushroomFormDialog mfd = new MushroomFormDialog(this, rootPaneCheckingEnabled, mushroom);
        mfd.pack();
        mfd.setVisible(true);
        return mfd.getMushroom();
    }

    /**
     * @param args the command line arguments
     */
    public void run() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        if (!System.getProperty("os.name").toLowerCase().contains("mac")) // if not on mac //NOI18N
        {
            try {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) { //NOI18N
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (ClassNotFoundException ex) {
                java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                setVisible(true);
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel MushroomHunterPanel;
    private javax.swing.JPanel MushroomPanel;
    private javax.swing.JTextPane OutputTextPane;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    private void getMushroomsByType(MushroomType mushroomType) {
        GetMushroomsByTypeSwingWorker gmbtsw = new GetMushroomsByTypeSwingWorker(serverUrl, mushroomType);
        OutputTextPane.setText(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("EXECUTING GET {0} MUSHROOMS..."), new Object[]{mushroomType}));
        gmbtsw.execute();
        try {
            OutputTextPane.setText(gmbtsw.get(workerTimeout, TimeUnit.MILLISECONDS).toString());
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            OutputTextPane.setText(java.util.ResourceBundle.getBundle("cz/muni/fi/pa165/restclient/i18n/default").getString("SERVER NOT RESPONDING."));
        }
        
    }

    private MushroomHunter getMushroomHunterByDialog(MushroomHunter mushroomHunter) {
        MushroomHunterFormDialog mfdDialog = new MushroomHunterFormDialog(this, rootPaneCheckingEnabled, mushroomHunter);
        mfdDialog.pack();
        mfdDialog.setVisible(true);
        return mfdDialog.getMushroomHunter();
    }
}
