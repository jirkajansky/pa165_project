/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.restclient.swing.workers.mushroom;

import cz.muni.fi.pa165.restclient.entities.Mushroom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.SwingWorker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Jan Bruzl
 */
public class GetAllMushroomsSwingWorker extends SwingWorker<List<Mushroom>, Object>{
    private String url;
    private final String restCode = "/rest/mushrooms/";
    

    public GetAllMushroomsSwingWorker(String url) {
        this.url = url;
    }
    
    @Override
    protected List<Mushroom> doInBackground() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String restUrl = url+restCode;
        ResponseEntity<Mushroom[]> responseEntity = restTemplate.getForEntity(restUrl, Mushroom[].class);
        List<Mushroom> mushrooms =  new ArrayList<>(Arrays.asList(responseEntity.getBody()));
        return mushrooms;
    }
    
}
