/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.restclient.swing.workers.mushroomHunter;

import cz.muni.fi.pa165.restclient.entities.MushroomHunter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.SwingWorker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Jan Bruzl
 */
public class GetAllMushroomsHunterSwingWorker extends SwingWorker<List<MushroomHunter>, Object>{
    private String url;
    private final String restCode = "/rest/hunters/";
    

    public GetAllMushroomsHunterSwingWorker(String url) {
        this.url = url;
    }
    
    @Override
    protected List<MushroomHunter> doInBackground() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String restUrl = url+restCode;
        ResponseEntity<MushroomHunter[]> responseEntity = restTemplate.getForEntity(restUrl, MushroomHunter[].class);
        List<MushroomHunter> mushroomHunters =  new ArrayList<>(Arrays.asList(responseEntity.getBody()));
        return mushroomHunters;
    }
    
}
