package cz.muni.fi.pa165.mushroomhunterportal.api.interfaces;

import cz.muni.fi.pa165.mushroomhunterportal.dao.UserDao;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 *
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
public interface UserDetailsServiceMHP extends UserDetailsService{

    void setUserDao(UserDao userDao);
}
