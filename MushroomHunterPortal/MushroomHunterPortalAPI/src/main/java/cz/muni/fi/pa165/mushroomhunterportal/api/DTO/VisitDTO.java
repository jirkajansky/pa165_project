package cz.muni.fi.pa165.mushroomhunterportal.api.DTO;

import java.util.Date;
import java.util.Objects;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * VisitDTO entity
 *
 * @author Jan Bruzl
 */
public class VisitDTO {

    private long id;   
    private long hunterId;
    private long forestId;
    private MushroomHunterDTO hunter;
    private ForestDTO forest;
    
    @DateTimeFormat(pattern="dd.MM.yyyy")
    private Date dateOfVisit;
    private String dateOfVisitStr;
    private String note;

    public VisitDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public long getHunterId() {
        return hunterId;
    }

    public void setHunterId(long hunterId) {
        this.hunterId = hunterId;
    }

    public long getForestId() {
        return forestId;
    }

    public void setForestId(long forestId) {
        this.forestId = forestId;
    }

    public MushroomHunterDTO getHunter() {
        return hunter;
    }

    public void setHunter(MushroomHunterDTO hunter) {
        this.hunter = hunter;
    }

    public ForestDTO getForest() {
        return forest;
    }

    public void setForest(ForestDTO forest) {
        this.forest = forest;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }
    
    public String getDateOfVisitStr() {
        return dateOfVisitStr;
    }

    public void setDateOfVisitStr(String dateOfVisitStr) {
        this.dateOfVisitStr = dateOfVisitStr;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Visit{" + "id=" + id + ", hunter=" + hunter + ", forest=" + forest + ", dateOfVisit=" + dateOfVisit + ", note=" + note + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 89 * hash + Objects.hashCode(this.hunter);
        hash = 89 * hash + Objects.hashCode(this.forest);
        hash = 89 * hash + Objects.hashCode(this.dateOfVisit);
        hash = 89 * hash + Objects.hashCode(this.note);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VisitDTO other = (VisitDTO) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.hunter, other.hunter)) {
            return false;
        }
        if (!Objects.equals(this.forest, other.forest)) {
            return false;
        }
        if (!Objects.equals(this.dateOfVisit, other.dateOfVisit)) {
            return false;
        }
        if (!Objects.equals(this.note, other.note)) {
            return false;
        }
        return true;
    }

}
