package cz.muni.fi.pa165.mushroomhunterportal.api.interfaces;

import cz.muni.fi.pa165.mushroomhunterportal.dao.ForestDao;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.ForestDTO;
import java.util.List;

/**
 *
 * @author Martina Prochazkova
 */
public interface ForestService {
    
        /**
     * Update given forest.
     * 
     * @param forest 
     */
    public void updateForest(ForestDTO forest);
    
    /**
     * Delete given forest.
     * 
     * @param forest 
     */
    public void deleteForest(ForestDTO forest);
    
    /**
     * Save new forest.
     * 
     * @param forest 
     */
    public void createForest(ForestDTO forest);
    
    /**
     * Returns forest with given id.
     * 
     * @param id
     * @return 
     */
    public ForestDTO getForestById(long id);
    
    /**
     * Returns list of all forests.
     * @return 
     */
    public List<ForestDTO> getAllForests();
            
    /**
     * Find forests which name like given parameter.
     * 
     * @param name
     * @return 
     */
    public List<ForestDTO> findForest(String name);
    
    /**
     * Set DAO
     * @param dao 
     */
    public void setDao(ForestDao dao);
    
}
