package cz.muni.fi.pa165.mushroomhunterportal.api.interfaces;

import cz.muni.fi.pa165.mushroomhunterportal.dao.MushroomDao;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomDTO;
import java.util.List;

/**
 *
 * @author Jan Bruzl
 */
public interface MushroomService {
    /**
     * Update given mushroom.
     * 
     * @param mushroom 
     */
    public void updateMushroom(MushroomDTO mushroom);
    
    /**
     * Delete given mushroom.
     * 
     * @param mushroom 
     */
    public void deleteMushroom(MushroomDTO mushroom);
    
    /**
     * Save new mushroom.
     * 
     * @param mushroom 
     */
    public void createMushroom(MushroomDTO mushroom);
    
    /**
     * Returns mushroom with given id.
     * 
     * @param id
     * @return 
     */
    public MushroomDTO getMushroomById(long id);
    
    /**
     * Returns list of all mushrooms.
     * @return 
     */
    public List<MushroomDTO> getAllMushrooms();
    
    /**
     * Return list of edible mushrooms.
     * 
     * @return 
     */
    public List<MushroomDTO> getEdibleMushrooms();
    
    /**
     * Return list of unedible mushrooms.
     * 
     * @return 
     */
    public List<MushroomDTO> getUnedibleMushrooms();
    
    /**
     * Return list of poisonous mushrooms.
     * 
     * @return 
     */
    public List<MushroomDTO> getPoisonousMushrooms();
    
    /**
     * Find mushrooms which name like given parameter.
     * 
     * @param name
     * @return 
     */
    public List<MushroomDTO> findMushroom(String name);
    
    /**
     * Set DAO
     * @param dao 
     */
    public void setDao(MushroomDao dao);
}
