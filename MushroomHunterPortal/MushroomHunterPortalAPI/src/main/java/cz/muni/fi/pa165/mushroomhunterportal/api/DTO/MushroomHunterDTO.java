/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.mushroomhunterportal.api.DTO;

import java.util.Objects;

/**
 * MushroomHunterDTO entity
 *
 * @author Jiri Jansky
 */
public class MushroomHunterDTO {
    private long id;

    private String firstName;

    private String surName;

    private String personalInfo;

    /**
     * 
     * @return id
     */
    public long getId() {
        return id;
    }
    
    /**
     * set id
     * @param id 
     */
    public void setId(long id) {
        this.id = id;
    }
    /**
     * 
     * @return all name 
     */
    public String getAllName()    
    {
        return firstName +" "+ surName;
    }

    /**
     * 
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }
    
    /**
     * set
     * @param firstName 
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * 
     * @return surname
     */
    public String getSurName() {
        return surName;
    }

    /**
     * set
     * @param surName 
     */
    public void setSurName(String surName) {
        this.surName = surName;
    }

    /**
     * 
     * @return personal info
     */
    public String getPersonalInfo() {
        return personalInfo;
    }

    /**
     * set
     * @param personalInfo 
     */
    public void setPersonalInfo(String personalInfo) {
        this.personalInfo = personalInfo;
    }

    @Override
    public String toString() {
        return "MushroomHunterDTO{" + "id=" + id + ", firstName=" + firstName + ", surName=" + surName + ", personalInfo=" + personalInfo + '}';
    }

     @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 83 * hash + Objects.hashCode(this.firstName);
        hash = 83 * hash + Objects.hashCode(this.surName);
        hash = 83 * hash + Objects.hashCode(this.personalInfo); 
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MushroomHunterDTO other = (MushroomHunterDTO) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.surName, other.surName)) {
            return false;
        }
        if (!Objects.equals(this.personalInfo, other.personalInfo)) {
            return false;
        }
        return true;
    }
    

}
