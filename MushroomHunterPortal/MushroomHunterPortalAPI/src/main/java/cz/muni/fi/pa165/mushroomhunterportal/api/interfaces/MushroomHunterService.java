package cz.muni.fi.pa165.mushroomhunterportal.api.interfaces;

import cz.muni.fi.pa165.mushroomhunterportal.dao.MushroomHunterDao;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import java.util.List;

/**
 *
 * @author Jiri Jansky
 */
public interface MushroomHunterService {
    
    /**
     * Persists new MushroomHunter entity.
     * 
     * @param hunter 
     */
    public void createHunter(MushroomHunterDTO hunter);
    
    /**
     * Update persisted MushroomHunter entity.
     * 
     * @param hunter 
     */
    public void updateHunter(MushroomHunterDTO hunter);
    
    /**
     * Delete persisted MushroomHunter entity.
     * 
     * @param hunter 
     */
    public void deleteHunter(MushroomHunterDTO hunter);
    
    /**
     * TODO
     * @param id
     * @return 
     */
    public MushroomHunterDTO getById(long id);
   
    /**
     * Returns all persisted MushroomHunter entities.
     * 
     * @return hunters
     */
    public List<MushroomHunterDTO> getAllHunters();
    
    /**
     * Returns List of hunters which name or surname contains searched string.
     * @param search
     * @return 
     */
    public List<MushroomHunterDTO> findHunter(String search);
    
    /**
     * 
     * @param dao 
     */
    public void setMushroomHunterDao(MushroomHunterDao dao);

}
