package cz.muni.fi.pa165.mushroomhunterportal.api.DTO;

/**
 * Mushroom type enum
 * TODO
 * @author Jan Bruzl
 */
public enum MushroomTypeDTO {
    EDIBLE, UNEDIBLE, POISONOUS
}
