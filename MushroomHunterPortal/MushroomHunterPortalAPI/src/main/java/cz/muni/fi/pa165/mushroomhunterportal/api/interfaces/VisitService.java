package cz.muni.fi.pa165.mushroomhunterportal.api.interfaces;

import cz.muni.fi.pa165.mushroomhunterportal.dao.VisitDao;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.ForestDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.VisitDTO;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jan Trnka
 */
public interface VisitService {

    /**
     * Save new visit.
     *
     * @param visit
     */
    public void createVisit(VisitDTO visit);

    /**
     * Delete given visit.
     *
     * @param visit
     */
    public void deleteVisit(VisitDTO visit);

    /**
     * Update given visit.
     *
     * @param visit
     */
    public void updateVisit(VisitDTO visit);

    /**
     * Returns visit by with given id.
     *
     * @param id
     * @return
     */
    public VisitDTO getVisitById(long id);

    /**
     * Returns list of all visits.
     *
     * @return collection of Visits
     */
    public List<VisitDTO> getAllVisits();

    /**
     * Returns list of visits to given forest.
     *
     * @param forest
     * @return
     */
    public List<VisitDTO> getVisitsByForest(ForestDTO forest);

    /**
     * Returns list of visits of given MushroomHunter.
     *
     * @param hunter
     * @return
     */
    public List<VisitDTO> getVisitsByHunter(MushroomHunterDTO hunter);

    /**
     * Find visits according to given parameters. Set null parameter to skip it
     * in search.
     *
     * @param afterDate
     * @param beforeDate
     * @param forest
     * @param mushroomHunter
     * @return
     */
    public List<VisitDTO> findVisit(Date afterDate, Date beforeDate, ForestDTO forest, MushroomHunterDTO mushroomHunter);

    /**
     * Set DAO
     *
     * @param dao
     */
    public void setDao(VisitDao dao);

}
