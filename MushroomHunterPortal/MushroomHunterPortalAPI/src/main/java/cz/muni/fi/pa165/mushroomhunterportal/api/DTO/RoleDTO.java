package cz.muni.fi.pa165.mushroomhunterportal.api.DTO;

/**
 * Defined roles for {@link UserDTO}
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
public enum RoleDTO {
    ROLE_USER, ROLE_ADMINISTRATOR;
}
