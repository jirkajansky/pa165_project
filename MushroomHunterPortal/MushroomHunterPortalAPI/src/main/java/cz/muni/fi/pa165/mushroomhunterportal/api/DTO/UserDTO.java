package cz.muni.fi.pa165.mushroomhunterportal.api.DTO;

/**
 * User data transfer object
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
public class UserDTO {
    private long id;
    private String mail;
    private long hunterId;
    private MushroomHunterDTO hunter;
    private RoleDTO role;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    
    public MushroomHunterDTO getHunter() {
        return hunter;
    }

    public void setHunter(MushroomHunterDTO hunterDTO) {
        this.hunter = hunterDTO;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    public long getHunterId() {
        return hunterId;
    }

    public void setHunterId(long hunterId) {
        this.hunterId = hunterId;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserDTO other = (UserDTO) obj;
        if (this.id != other.id){
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserDTO{" + "id=" + id + ", mail=" + mail + ", hunterDTO=" + hunter + ", role=" + role + '}';
    }

    public UserDTO() {
        this.id = 0L; 
        hunterId = 0L;
        
    }

    public UserDTO(Long id, String mail, MushroomHunterDTO hunterDTO, RoleDTO role) {
        this.id = id;
        this.mail = mail;
        this.hunter = hunterDTO;
        this.role = role;
    }
}
