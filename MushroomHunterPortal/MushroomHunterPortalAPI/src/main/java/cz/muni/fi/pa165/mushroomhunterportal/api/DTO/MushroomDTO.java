package cz.muni.fi.pa165.mushroomhunterportal.api.DTO;

import java.util.Objects;


/**
 * MushroomDTO entity
 * @author Jan Bruzl
 */
public class MushroomDTO {
    private long id;
    
    private String name;
    
    private MushroomTypeDTO type;
    
    private String occurence;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MushroomTypeDTO getType() {
        return type;
    }

    public void setType(MushroomTypeDTO type) {
        this.type = type;
    }

    public String getOccurence() {
        return occurence;
    }

    public void setOccurence(String occurence) {
        this.occurence = occurence;
    }

    @Override
    public String toString() {
        return "MushroomDTO{" + "id=" + id + ", name=" + name + ", type=" + type + ", occurence=" + occurence + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.type);
        hash = 97 * hash + Objects.hashCode(this.occurence);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MushroomDTO other = (MushroomDTO) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.occurence, other.occurence)) {
            return false;
        }
        return true;
    }

    
    
}
