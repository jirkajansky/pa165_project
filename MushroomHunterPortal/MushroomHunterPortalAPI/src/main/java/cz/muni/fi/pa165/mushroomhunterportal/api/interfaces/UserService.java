package cz.muni.fi.pa165.mushroomhunterportal.api.interfaces;

import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.UserDTO;
import cz.muni.fi.pa165.mushroomhunterportal.dao.UserDao;
import java.util.List;

/**
 * Basic service for {@link UserDTO}
 *
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
public interface UserService {

    /**
     * Create new {@link UserDTO}
     *
     * @param userDTO {@link UserDTO}
     * @param password hashed password for user
     */
    public void createUser(UserDTO userDTO, String password);

    /**
     * Update given {@link UserDTO}
     *
     * @param userDTO {@link UserDTO}
     */
    public void updateUser(UserDTO userDTO);

    /**
     * Delete given {@link UserDTO}
     *
     * @param userDTO {@link UserDTO}
     */
    public void deleteUser(UserDTO userDTO);

    /**
     * Get {@link UserDTO} by given id
     *
     * @param id {@link Long}
     * @return {@link UserDTO}
     */
    public UserDTO getById(Long id);

    /**
     * Get {@link UserDTO} with given mail
     *
     * @param mail {@link String}
     * @return {@link UserDTO}
     */
    public UserDTO getByMail(String mail);

    /**
     * get logged user
     * @return user
     */
    public UserDTO getLogedUser();
    
    /**
     * Returns all {@link UserDTO}
     *
     * @return {@link List<UserDTO>}
     */
    public List<UserDTO> getAll();

    /**
     * Set password for given user. Password is encrypted in process.
     *
     * @param userDTO
     * @param password
     */
    public void setPassword(UserDTO userDTO, String password);

    /**
     * Returns user connected to passed hunter
     * @param hunter
     * @return user
     */
    UserDTO getByHunter(MushroomHunterDTO hunter);

    /**
     * Checks if user identified by passed mail already exists.
     * @param mail
     * @return mailIsTaken
     */
    public boolean checkMailIsAlreadyTaken(String mail);
    
    
    /**
     * set dao for test purpose
     * @param dao 
     */
    public void setDao(UserDao dao);

}
