package cz.muni.fi.pa165.mushroomhunterportal.service.tests;

import cz.muni.fi.pa165.mushroomhunterportal.dao.MushroomDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Mushroom;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomDTO;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomType;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomTypeDTO;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceException;
import org.dozer.Mapper;
import org.junit.Assert;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.security.test.context.support.WithSecurityContextTestExcecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;

/**
 *
 * @author Martina Prochazkova
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:serviceApplicationContext.xml"})
@TestExecutionListeners(listeners={ServletTestExecutionListener.class,
		DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		WithSecurityContextTestExcecutionListener.class})
public class MushroomServiceTest {
    @Autowired
    private MushroomService mushroomService;
    
    @Mock
    private MushroomDao dao;

    @Autowired 
    Mapper dozerBeanMapper;
    
    List<Mushroom> edibleMushrooms = new ArrayList<>();
    List<Mushroom> unedibleMushrooms = new ArrayList<>();
    List<Mushroom> poisonousMushrooms = new ArrayList<>();
    
    @Before
    public void setUp(){
        
        Mushroom m1 = new Mushroom();
        Mushroom m2 = new Mushroom();
        Mushroom m3 = new Mushroom();
        Mushroom m4 = new Mushroom();

        m1.setId(1);
        m2.setId(2);
        m3.setId(3);
        m4.setId(4);

        m1.setName("Mushroom 1");
        m2.setName("Mushroom 2");
        m3.setName("Mushroom 3");
        m4.setName("Mushroom 4");
        
        m1.setType(MushroomType.EDIBLE);
        m2.setType(MushroomType.UNEDIBLE);
        m3.setType(MushroomType.POISONOUS);
        m4.setType(MushroomType.EDIBLE);
        
        List<Mushroom> mushrooms = new ArrayList<>();
        mushrooms.add(m1);
        mushrooms.add(m2);
        mushrooms.add(m3);
        mushrooms.add(m4);
        
        
        edibleMushrooms.add(m1);
        unedibleMushrooms.add(m2);
        poisonousMushrooms.add(m3);
        edibleMushrooms.add(m4);
        
        dao = Mockito.mock(MushroomDao.class);

        Mockito.when(dao.getAllMushrooms()).thenReturn(mushrooms);
        Mockito.when(dao.getById(eq(1L))).thenReturn(m1);
        Mockito.when(dao.getById(eq(2L))).thenReturn(m2);
        Mockito.when(dao.getById(eq(3L))).thenReturn(m3);
        Mockito.when(dao.getById(eq(4L))).thenReturn(m4);
        Mockito.when(dao.getById(not(or(or(eq(1L), eq(2L)), or(eq(3L), eq(4L)))))).thenReturn(null);
        
        Mockito.doThrow(new PersistenceException()).when(dao).deleteMushroom(m4);
        Mockito.doThrow(new PersistenceException()).when(dao).updateMushroom(m4);
        Mockito.doThrow(new PersistenceException()).when(dao).addMushroom(m4);       
        
        mushroomService.setDao(dao);
    }

    @Test
    public void basicTest() {
        Assert.assertNotNull(mushroomService);
        Assert.assertNotNull(dao);
        Assert.assertNotNull(dozerBeanMapper);
    }

    @Test(expected = DataAccessException.class)
    public void createTest() {
        MushroomDTO mushroomDTO = new MushroomDTO();
        mushroomDTO.setId(5);
        mushroomDTO.setName("Mushroom 5");
        mushroomDTO.setType(MushroomTypeDTO.EDIBLE);

        Mushroom mushroomEntity = dozerBeanMapper.map(mushroomDTO, Mushroom.class);

        mushroomService.createMushroom(mushroomDTO);

        Mockito.verify(dao).addMushroom(mushroomEntity);
        
        MushroomDTO exceptionMushroom = getExceptionMushroom();
        mushroomService.createMushroom(exceptionMushroom);
    }

    @Test(expected = DataAccessException.class)
    public void updateTest() {
        MushroomDTO mushroomDTO = new MushroomDTO();
        mushroomDTO.setId(1);
        mushroomDTO.setName("Mushroom 1 updated");
        mushroomDTO.setType(MushroomTypeDTO.UNEDIBLE);;

        Mushroom mushroomEntity = dozerBeanMapper.map(mushroomDTO, Mushroom.class);

        mushroomService.updateMushroom(mushroomDTO);

        Mockito.verify(dao).updateMushroom(mushroomEntity);
        
        MushroomDTO exceptionMushroom = getExceptionMushroom();
        mushroomService.updateMushroom(exceptionMushroom);
    }

    @Test(expected = DataAccessException.class)
    public void deleteTest() {
        MushroomDTO mushroomDTO = new MushroomDTO();
        mushroomDTO.setId(1);
        mushroomDTO.setName("Mushroom 1");
        mushroomDTO.setType(MushroomTypeDTO.POISONOUS);

        Mushroom mushroomEntity = dozerBeanMapper.map(mushroomDTO, Mushroom.class);

        mushroomService.deleteMushroom(mushroomDTO);

        Mockito.verify(dao).deleteMushroom(mushroomEntity);
       
        MushroomDTO exceptionMushroom = getExceptionMushroom();
        mushroomService.deleteMushroom(exceptionMushroom);
    }

    @Test
    public void getAllTest() {
        List<MushroomDTO> mushrooms = mushroomService.getAllMushrooms();

        Mockito.verify(dao).getAllMushrooms();

        Assert.assertNotNull("Returned list should never be null.", mushrooms);

        final List<Mushroom> daoMushrooms = dao.getAllMushrooms();

        Assert.assertEquals("Expected list of size " + daoMushrooms.size() + ", obtained list with size: " + mushrooms.size(),
                daoMushrooms.size(), mushrooms.size());

        for (Mushroom mushroom : daoMushrooms) {
            MushroomDTO mushroomFromDao = dozerBeanMapper.map(mushroom, MushroomDTO.class);
            Assert.assertTrue("Mushroom " + mushroom + " not found in service", mushrooms.contains(mushroomFromDao));
        }

    }
    
        @Test
    public void getByIdTest() {
        MushroomDTO visit3DTO = mushroomService.getMushroomById(3);

        Mockito.verify(dao).getById(3);

        MushroomDTO mushroom3FromDaoDTO
                = dozerBeanMapper.map(dao.getById(3), MushroomDTO.class);

        Assert.assertEquals(visit3DTO, mushroom3FromDaoDTO);
    }
    
    @Test
    public void getTypesTest() {
        // edible test
        List<MushroomDTO> mushroomsEdible = mushroomService.getEdibleMushrooms();

        Assert.assertEquals("Expected list of size " + edibleMushrooms.size() + ", obtained list with size: " + mushroomsEdible.size(),
                edibleMushrooms.size(), mushroomsEdible.size());

        for (Mushroom mushroom : edibleMushrooms) {
            MushroomDTO mushroomFromDao = dozerBeanMapper.map(mushroom, MushroomDTO.class);
            Assert.assertTrue("Mushroom " + mushroom + " not found in service", mushroomsEdible.contains(mushroomFromDao));
        }
        
        // unedible test
        List<MushroomDTO> mushroomsUnedible = mushroomService.getUnedibleMushrooms();

        Assert.assertEquals("Expected list of size " + unedibleMushrooms.size() + ", obtained list with size: " + mushroomsUnedible.size(),
                unedibleMushrooms.size(), mushroomsUnedible.size());

        for (Mushroom mushroom : unedibleMushrooms) {
            MushroomDTO mushroomFromDao = dozerBeanMapper.map(mushroom, MushroomDTO.class);
            Assert.assertTrue("Mushroom " + mushroom + " not found in service", mushroomsUnedible.contains(mushroomFromDao));
        }
        
        // poisonous test
        List<MushroomDTO> mushroomsPoisonous = mushroomService.getPoisonousMushrooms();

        Assert.assertEquals("Expected list of size " + poisonousMushrooms.size() + ", obtained list with size: " + mushroomsPoisonous.size(),
                poisonousMushrooms.size(), mushroomsPoisonous.size());

        for (Mushroom mushroom : poisonousMushrooms) {
            MushroomDTO mushroomFromDao = dozerBeanMapper.map(mushroom, MushroomDTO.class);
            Assert.assertTrue("Mushroom " + mushroom + " not found in service", mushroomsPoisonous.contains(mushroomFromDao));
        }

    }

    @Test
    public void findTest() {
        MushroomDTO mushroomDTO = new MushroomDTO();
        mushroomDTO.setId(1);
        mushroomDTO.setName("Mushroom 1");
        mushroomDTO.setType(MushroomTypeDTO.EDIBLE);

        List<MushroomDTO> mushrooms = mushroomService.findMushroom(mushroomDTO.getName());

        Mockito.verify(dao).getAllMushrooms();

        Assert.assertNotNull("Returned list should never be null.", mushrooms);

        Assert.assertEquals("Expected list of size 1, obtained list with size: " + mushrooms.size(), 1, mushrooms.size());

        Assert.assertEquals(mushroomDTO, mushrooms.get(0));
        
        mushrooms = mushroomService.findMushroom("Mushroom");

        Assert.assertNotNull("Returned list should never be null.", mushrooms);

        Assert.assertEquals("Expected list of size 4, obtained list with size: " + mushrooms.size(), 4, mushrooms.size());
    }
    
    /**
     * Return MushroomDTO that was set to cause PersistenceException in mocked dao.
     * 
     * @return MushroomDTO
     */
    private MushroomDTO getExceptionMushroom() {
        Mushroom m4 = new Mushroom();
        m4.setId(4);
        m4.setName("Mushroom 4");
        return dozerBeanMapper.map(m4, MushroomDTO.class);
    }
}
