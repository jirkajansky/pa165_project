package cz.muni.fi.pa165.mushroomhunterportal.service.tests;

import cz.muni.fi.pa165.mushroomhunterportal.dao.MushroomHunterDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomHunterService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceException;
import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithSecurityContextTestExcecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;

/**
 *
 * @author Jiří Jánský
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:serviceApplicationContext.xml"})
@TestExecutionListeners(listeners={ServletTestExecutionListener.class,
		DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		WithSecurityContextTestExcecutionListener.class})
public class MushroomHunterServiceTest {
    @Mock   
    private MushroomHunterDao dao;   
    
    @Autowired
    Mapper dozerBeanMapper;
    
    @Autowired
    private MushroomHunterService mushroomHunterService;
    
    @Before
    public void setUp()
    {
        List<MushroomHunter> hunters = new ArrayList<>();
        MushroomHunter h1 = new MushroomHunter();
        h1.setFirstName("Filip");
        h1.setSurName("Kral");
        h1.setPersonalInfo("tajny hubar;"); 
        MushroomHunter h2 = new MushroomHunter();
        h2.setFirstName("Petr");
        h2.setSurName("Bedna");
        h2.setPersonalInfo("rad hribky;"); 
        MushroomHunter h3 = new MushroomHunter();
        h3.setFirstName("Karel");
        h3.setSurName("Lak");
        h3.setPersonalInfo("boji se tmy;"); 

        h1.setId(1);
        h2.setId(2);
        h3.setId(3);
        
        hunters.add(h1);
        hunters.add(h2);
        hunters.add(h3);
        hunters.add(getExceptionHunter());
        
        dao = Mockito.mock(MushroomHunterDao.class);  
        Mockito.when(dao.getAllHunters()).thenReturn(hunters);
        Mockito.when(dao.getById(eq(1L))).thenReturn(h1);
        Mockito.when(dao.getById(eq(2L))).thenReturn(h2);
        Mockito.when(dao.getById(eq(3L))).thenReturn(h3);
        Mockito.when(dao.getById(eq(4L))).thenReturn(getExceptionHunter());
        Mockito.when(dao.getById(not(or(or(eq(1L), eq(2L)), or(eq(3L), eq(4L)))))).thenReturn(null);
        
        Mockito.doThrow(new PersistenceException()).when(dao).deleteHunter(getExceptionHunter());
        Mockito.doThrow(new PersistenceException()).when(dao).updateHunter(getExceptionHunter());
        Mockito.doThrow(new PersistenceException()).when(dao).addHunter(getExceptionHunter());
        
        mushroomHunterService.setMushroomHunterDao(dao);
    }
    
    private MushroomHunter getExceptionHunter(){
        MushroomHunter exceptionMH = new MushroomHunter();
        exceptionMH.setFirstName("Ondra");
        exceptionMH.setSurName("Fazole");
        exceptionMH.setPersonalInfo("sbira holubenky;");
        exceptionMH.setId(4);
        
        return exceptionMH;
    }
    
    @Test
    public void basicTest() {
        Assert.assertNotNull(mushroomHunterService);
        Assert.assertNotNull(dao);
    }
    
    @Test(expected = DataAccessException.class)
    public void createTest()
    {
        MushroomHunterDTO hunterDTO = new MushroomHunterDTO();
        hunterDTO.setFirstName("Filip");
        hunterDTO.setSurName("Kral");
        hunterDTO.setPersonalInfo("tajny hubar;rad hribky");        
        MushroomHunter hunter = dozerBeanMapper.map(hunterDTO,MushroomHunter.class);
        
        mushroomHunterService.createHunter(hunterDTO);
        
        Mockito.verify(dao).addHunter(hunter);   
        
        mushroomHunterService.createHunter(dozerBeanMapper.map(getExceptionHunter(), MushroomHunterDTO.class));
    }
    
    @Test(expected = DataAccessException.class)
    @WithMockUser(username="test",roles={"USER","ADMINISTRATOR"})
    public void updateTest()
    {
        MushroomHunterDTO hunterDTO = new MushroomHunterDTO();
        hunterDTO.setFirstName("Filip");
        hunterDTO.setSurName("Kral");
        hunterDTO.setPersonalInfo("tajny hubar;rad hribky;");                      
        mushroomHunterService.createHunter(hunterDTO);        
        hunterDTO.setPersonalInfo(hunterDTO.getPersonalInfo()+"boji se po tme;");
        MushroomHunter hunter = dozerBeanMapper.map(hunterDTO,MushroomHunter.class);        
        mushroomHunterService.updateHunter(hunterDTO);        
        Mockito.verify(dao).updateHunter(hunter);      
        
        mushroomHunterService.updateHunter(dozerBeanMapper.map(getExceptionHunter(), MushroomHunterDTO.class));
    }
    
    @Test(expected = DataAccessException.class) 
    public void deleteTest()
    {
        MushroomHunterDTO hunterDTO = new MushroomHunterDTO();
        hunterDTO.setFirstName("Filip");
        hunterDTO.setSurName("Kral");
        hunterDTO.setPersonalInfo("tajny hubar;rad hribky;");                      
        mushroomHunterService.createHunter(hunterDTO);        
        MushroomHunter hunter = dozerBeanMapper.map(hunterDTO,MushroomHunter.class);        
        mushroomHunterService.deleteHunter(hunterDTO);        
        Mockito.verify(dao).deleteHunter(hunter);      
        
        mushroomHunterService.deleteHunter(dozerBeanMapper.map(getExceptionHunter(), MushroomHunterDTO.class));
    }
    
    @Test 
    public void getAllTest()
    {                       
        List<MushroomHunterDTO> mushroomHunterDTOs = mushroomHunterService.getAllHunters();
        Mockito.verify(dao).getAllHunters();
        Assert.assertNotNull("Returned list should never be null.", mushroomHunterDTOs);

        final List<MushroomHunter> hunterDaos = dao.getAllHunters();

        Assert.assertEquals("Expected list of size " + hunterDaos.size() + ", obtained list with size: " + mushroomHunterDTOs.size(),
                hunterDaos.size(), mushroomHunterDTOs.size());
        for (MushroomHunter hunter : hunterDaos) {
            MushroomHunterDTO tmpHunterDTO = dozerBeanMapper.map(hunter, MushroomHunterDTO.class);
            Assert.assertTrue("Hunter " + hunter + " not found in service", mushroomHunterDTOs.contains(tmpHunterDTO));
        }
    }
    @Test
    public void getByIdTest() {
        MushroomHunterDTO visit3DTO = mushroomHunterService.getById(3);

        Mockito.verify(dao).getById(3);

        MushroomHunterDTO hunter3FromDaoDTO
                = dozerBeanMapper.map(dao.getById(3), MushroomHunterDTO.class);

        Assert.assertEquals(visit3DTO, hunter3FromDaoDTO);
    }
    @Test
    public void findTest() {
        MushroomHunterDTO hunterDTO = new MushroomHunterDTO();        
        hunterDTO.setId(3L);
        hunterDTO.setFirstName("Karel");
        hunterDTO.setSurName("Lak");
        hunterDTO.setPersonalInfo("boji se tmy;"); 

        List<MushroomHunterDTO> hunters =  mushroomHunterService.findHunter("Karel");

        Mockito.verify(dao).getAllHunters();

        Assert.assertNotNull("Returned list should never be null.", hunters);

        Assert.assertEquals("Expected list of size 1, obtained list with size: " + hunters.size(), 1, hunters.size());

        Assert.assertEquals(hunterDTO, hunters.get(0));
        

        Assert.assertNotNull("Returned list should never be null.", hunters);
       
    }
    
}
