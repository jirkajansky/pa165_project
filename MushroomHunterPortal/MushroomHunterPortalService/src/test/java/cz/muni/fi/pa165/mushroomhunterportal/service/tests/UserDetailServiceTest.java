/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.mushroomhunterportal.service.tests;

import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.UserDetailsServiceMHP;
import cz.muni.fi.pa165.mushroomhunterportal.dao.UserDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Role;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.User;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithSecurityContextTestExcecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;

/**
 *
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:serviceApplicationContext.xml"})
@TestExecutionListeners(listeners={ServletTestExecutionListener.class,
		DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		WithSecurityContextTestExcecutionListener.class})
public class UserDetailServiceTest {
     @Autowired
    private UserDetailsServiceMHP serviceMHPImpl;
    
    @Autowired
    private UserDao dao;
    
    @Before
    public void setUp(){
        dao = Mockito.mock(UserDao.class);
        
        User user = getUser();
        
        Mockito.when(dao.getByMail(user.getMail())).thenReturn(user);
        
        serviceMHPImpl.setUserDao(dao);
    }

    protected User getUser() {
        User user = new User();
        user.setId(1L);
        user.setMail("a@b.com");
        user.setRole(Role.ROLE_USER);
        user.setPassword("a#");
        return user;
    }
    
    @Test
    public void loadUserByUsernameTest(){
        Assert.assertNotNull(serviceMHPImpl);
        
        UserDetails ud = serviceMHPImpl.loadUserByUsername("a@b.com");
        User u = getUser();
        Assert.assertEquals(u.getMail(), ud.getUsername());
        Assert.assertEquals(u.getPassword(), ud.getPassword());
        Assert.assertEquals(1,ud.getAuthorities().size());
        
        List<GrantedAuthority> ga = new ArrayList<>(ud.getAuthorities());
        Assert.assertEquals(Role.ROLE_USER.toString(), ga.get(0).getAuthority());
    }
    
}
