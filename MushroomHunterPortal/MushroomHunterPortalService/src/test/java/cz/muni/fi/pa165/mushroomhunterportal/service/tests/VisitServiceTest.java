package cz.muni.fi.pa165.mushroomhunterportal.service.tests;

import cz.muni.fi.pa165.mushroomhunterportal.dao.VisitDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Forest;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Visit;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.VisitService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.VisitDTO;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithSecurityContextTestExcecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;

/**
 * Tests for VisitService
 *
 * @author Jan Trnka
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:serviceApplicationContext.xml"})
@TestExecutionListeners(listeners = {ServletTestExecutionListener.class,
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    WithSecurityContextTestExcecutionListener.class})
@WithMockUser(username = "test", roles = {"ADMINISTRATOR"})
public class VisitServiceTest {

    @Autowired
    private VisitService visitService;

    @Mock
    private VisitDao dao;

    @Autowired
    Mapper dozerBeanMapper;

    @Before
    public void setUp() {
        Visit v1 = new Visit();
        Visit v2 = new Visit();
        Visit v3 = new Visit();
        Visit v4 = new Visit();
        Visit v5 = new Visit();

        v1.setId(1);
        v2.setId(2);
        v3.setId(3);
        v4.setId(4);
        v5.setId(5);
        v1.setNote("Visit 1");
        v2.setNote("Visit 2");
        v3.setNote("Visit 3");
        v4.setNote("Visit 4");

        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(2014, 10, 1);
        v1.setDateOfVisit(cal.getTime());

        cal.set(2014, 10, 2);
        v2.setDateOfVisit(cal.getTime());

        cal.set(2014, 10, 3);
        v3.setDateOfVisit(cal.getTime());

        cal.set(2014, 10, 4);
        v4.setDateOfVisit(cal.getTime());

        cal.set(2014, 10, 5);
        v5.setDateOfVisit(cal.getTime());

        MushroomHunter mh1 = new MushroomHunter();
        mh1.setFirstName("MH1F");
        mh1.setSurName("MH1S");
        mh1.setId(1);
        Set<Visit> visitsMH1 = new HashSet<>();
        visitsMH1.add(v1);
        visitsMH1.add(v2);
        visitsMH1.add(v5);
        mh1.setVisits(visitsMH1);
        mh1.setPersonalInfo("MH1Pinfo");

        MushroomHunter mh2 = new MushroomHunter();
        mh2.setFirstName("MH2F");
        mh2.setSurName("MH2S");
        mh2.setId(2);
        Set<Visit> visitsMH2 = new HashSet<>();
        visitsMH2.add(v3);
        visitsMH2.add(v4);
        mh2.setVisits(visitsMH2);
        mh2.setPersonalInfo("MH2Pinfo");

        Forest f1 = new Forest();
        f1.setDescription("Forest 1 DESC");
        f1.setName("Forest 1");
        f1.setLatitude(1);
        f1.setLongitude(1);
        f1.setVisits(visitsMH1);
        f1.setId(1);

        Forest f2 = new Forest();
        f2.setDescription("Forest 2 DESC");
        f2.setName("Forest 2");
        f2.setLatitude(2);
        f2.setLongitude(2);
        f2.setVisits(visitsMH2);
        f2.setId(2);

        v1.setHunter(mh1);
        v2.setHunter(mh1);
        v3.setHunter(mh2);
        v4.setHunter(mh2);
        v5.setHunter(mh1);

        v1.setForest(f1);
        v2.setForest(f1);
        v3.setForest(f2);
        v4.setForest(f2);
        v5.setForest(f1);

        List<Visit> visits = new ArrayList<>();
        visits.add(v1);
        visits.add(v2);
        visits.add(v3);
        visits.add(v4);
        visits.add(v5);
        dao = Mockito.mock(VisitDao.class);

        Mockito.when(dao.getAllVisit()).thenReturn(visits);
        Mockito.when(dao.getById(1)).thenReturn(v1);
        Mockito.when(dao.getById(2)).thenReturn(v2);
        Mockito.when(dao.getById(3)).thenReturn(v3);
        Mockito.when(dao.getById(4)).thenReturn(v4);
        Mockito.when(dao.getById(not(or(or(eq(1L), eq(2L)), or(eq(3L), eq(4L)))))).thenReturn(null);
        visitService.setDao(dao);
    }

    @Test
    public void basicTest() {
        Assert.assertNotNull(visitService);
        Assert.assertNotNull(dao);
        Assert.assertNotNull(dozerBeanMapper);
    }

    @Test
    public void createTest() {
        VisitDTO existingVisitDTO = visitService.getVisitById(1);
        VisitDTO visitDTO = new VisitDTO();
        visitDTO.setId(6);
        visitDTO.setNote("Visit 6");
        visitDTO.setForest(existingVisitDTO.getForest());
        visitDTO.setHunter(existingVisitDTO.getHunter());

        Visit visitEntity = dozerBeanMapper.map(visitDTO, Visit.class);

        visitService.createVisit(visitDTO);

        Mockito.verify(dao).addVisit(visitEntity);
    }

    @Test
    public void updateTest() {
        VisitDTO visitDTO = visitService.getVisitById(1);
        visitDTO.setNote("Visit 1 UPDATED");

        Visit visitEntity = dozerBeanMapper.map(visitDTO, Visit.class);

        visitService.updateVisit(visitDTO);

        Mockito.verify(dao).updateVisit(visitEntity);
    }

    @Test
    public void deleteTest() {
        Visit visit2 = dao.getById(2);
        VisitDTO visitToDeleteDTO
                = dozerBeanMapper.map(visit2, VisitDTO.class);

        visitService.deleteVisit(visitToDeleteDTO);

        Mockito.verify(dao).deleteVisit(visit2);
    }

    @Test
    public void getAllTest() {
        List<VisitDTO> visits = visitService.getAllVisits();

        Mockito.verify(dao).getAllVisit();

        Assert.assertNotNull("Returned list should never be null.", visits);

        final List<Visit> daoVisits = dao.getAllVisit();

        Assert.assertEquals("Expected list of size " + daoVisits.size() + ", obtained list of size: " + visits.size(),
                daoVisits.size(), visits.size());
        for (Visit v : daoVisits) {
            VisitDTO visitFromDaoDTO = dozerBeanMapper.map(v, VisitDTO.class);
            Assert.assertTrue("Visit " + v + " not found in service.", visits.contains(visitFromDaoDTO));
        }

    }

    @Test
    public void getByIdTest() {
        VisitDTO visit3DTO = visitService.getVisitById(3);

        Mockito.verify(dao).getById(3);

        VisitDTO visit3FromDaoDTO
                = dozerBeanMapper.map(dao.getById(3), VisitDTO.class);

        Assert.assertEquals(visit3DTO, visit3FromDaoDTO);
    }

    @Test
    public void getByHunter() {
        MushroomHunterDTO mhDTO = dozerBeanMapper
                .map(dao.getById(1).getHunter(), MushroomHunterDTO.class);

        List<VisitDTO> visitsFromMH1 = visitService.getVisitsByHunter(mhDTO);
        Assert.assertEquals("Expected 3 visits, got: " + visitsFromMH1.size(), 3, visitsFromMH1.size());
        for (VisitDTO vDTO : visitsFromMH1) {
            Visit v = dozerBeanMapper.map(vDTO, Visit.class);
            Assert.assertTrue("Only visits with id == 1,2,5 rxpected", v.getId() == 1 || v.getId() == 2 || v.getId() == 5);
        }

    }

    @Test(expected = IllegalArgumentException.class)
    public void NoRelationsCreateTest() {
        VisitDTO failVisitDTO = new VisitDTO();
        visitService.createVisit(failVisitDTO);
    }

    @Test(expected = IllegalArgumentException.class)
    public void NoRelationsUpdateTest() {
        VisitDTO failVisitDTO = new VisitDTO();
        visitService.updateVisit(failVisitDTO);
    }

    @Test(expected = IllegalArgumentException.class)
    public void NoRelationsDeleteTest() {
        VisitDTO failVisitDTO = new VisitDTO();
        visitService.deleteVisit(failVisitDTO);
    }

    @Test(expected = IllegalArgumentException.class)
    public void NullArgumentCreateTest() {
        visitService.createVisit(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void NullArgumentUpdateTest() {
        visitService.updateVisit(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void NullArgumentDeleteTest() {
        visitService.deleteVisit(null);
    }
}
