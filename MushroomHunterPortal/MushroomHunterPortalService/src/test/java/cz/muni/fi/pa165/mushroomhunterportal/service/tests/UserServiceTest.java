package cz.muni.fi.pa165.mushroomhunterportal.service.tests;

import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.RoleDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.User;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.UserDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.UserService;
import cz.muni.fi.pa165.mushroomhunterportal.dao.UserDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Role;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceException;
import org.dozer.Mapper;
import org.junit.Assert;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithSecurityContextTestExcecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;

/**
 *
 * @author Jiri Jansky
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:serviceApplicationContext.xml"})
@TestExecutionListeners(listeners = {ServletTestExecutionListener.class,
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    WithSecurityContextTestExcecutionListener.class})
@WithMockUser(username = "test", roles = {"ADMINISTRATOR"})
public class UserServiceTest {
    @Autowired
    private UserService userService;
    
    @Mock
    private UserDao dao;

    @Autowired 
    Mapper dozerBeanMapper;
    

    
    @Before
    public void setUp(){
        
        User u1 = new User();
        User u2 = new User();
        User u3 = new User();
        User u4 = new User();

        u1.setId(1L);
        u2.setId(2L);
        u3.setId(3L);
        u4.setId(4L);

        u1.setMail("User 1");
        u2.setMail("User 2");
        u3.setMail("User 3");
        u4.setMail("User 4");
        
        u1.setPassword("1");
        u2.setPassword("2");
        u3.setPassword("3");
        u4.setPassword("4");
        
        u1.setRole(Role.ROLE_USER);
        u2.setRole(Role.ROLE_USER);
        u3.setRole(Role.ROLE_USER);
        u4.setRole(Role.ROLE_USER);
        
        List<User> users = new ArrayList<>();
        users.add(u1);
        users.add(u2);
        users.add(u3);
        users.add(u4);
        
        
        dao = Mockito.mock(UserDao.class);

        Mockito.when(dao.findAll()).thenReturn(users);
        Mockito.when(dao.getByMail("User 1")).thenReturn(u1);
        Mockito.when(dao.getByMail("User 2")).thenReturn(u2);
        Mockito.when(dao.getByMail("User 3")).thenReturn(u3);
        Mockito.when(dao.getByMail("User 4")).thenReturn(u4);
        
        
        Mockito.doThrow(new PersistenceException()).when(dao).delete(u4);
        Mockito.doThrow(new PersistenceException()).when(dao).save(u4);
        Mockito.doThrow(new PersistenceException()).when(dao).save(u4);       
        
        userService.setDao(dao);
    }

    @Test
    public void basicTest() {
        Assert.assertNotNull(userService);
        Assert.assertNotNull(dao);
        Assert.assertNotNull(dozerBeanMapper);
    }

    @Test(expected = DataAccessException.class)
    public void createTest() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(5L);
        userDTO.setMail("User 5");
        userDTO.setRole(RoleDTO.ROLE_USER);

        User user = dozerBeanMapper.map(userDTO, User.class);

        userService.createUser(userDTO,"5");

        Mockito.verify(dao).save(user);
        
        UserDTO exceptionUser = getExceptionUser();
        userService.createUser(exceptionUser,"5");
    }

    @Test(expected = DataAccessException.class)
    public void updateTest() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);
        userDTO.setMail("User 1");
        userDTO.setRole(RoleDTO.ROLE_ADMINISTRATOR);

        User userEntity = dozerBeanMapper.map(userDTO, User.class);

        userService.updateUser(userDTO);

        Mockito.verify(dao).save(userEntity);
        
        UserDTO exceptionUser = getExceptionUser();
        userService.updateUser(exceptionUser);
    }

    @Test(expected = DataAccessException.class)
    public void deleteTest() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);
        userDTO.setMail("User 1");
        userDTO.setRole(RoleDTO.ROLE_USER);

        User userEntity = dozerBeanMapper.map(userDTO, User.class);

        userService.deleteUser(userDTO);

        Mockito.verify(dao).delete(userEntity);
       
        UserDTO exceptionUser = getExceptionUser();
        userService.deleteUser(exceptionUser);
    }

    @Test
    public void getAllTest() {
        List<UserDTO> users = userService.getAll();

        Mockito.verify(dao).findAll();

        Assert.assertNotNull("Returned list should never be null.", users);

        List<User> daoUsers = new ArrayList<User>();
        for (User user: dao.findAll())
        {
            daoUsers.add(user);
        }

        Assert.assertEquals("Expected list of size " + daoUsers.size() + ", obtained list with size: " + users.size(),
                daoUsers.size(), users.size());

        for (User user : daoUsers) {
            UserDTO userFromDao = dozerBeanMapper.map(user, UserDTO.class);
            Assert.assertTrue("User " + user + " not found in service", users.contains(userFromDao));
        }

    }
    
        @Test
    public void getByIdTest() {
        UserDTO visit3DTO = userService.getById(3L);

    }
   

    @Test
    public void findTest() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);
        userDTO.setMail("User 1");
        userDTO.setRole(RoleDTO.ROLE_USER);

        UserDTO user = userService.getByMail(userDTO.getMail());

        Mockito.verify(dao).getByMail(userDTO.getMail());

        Assert.assertNotNull("Returned list should never be null.", user);
        

        Assert.assertEquals(userDTO, user);
    }
    
    /**
     * Return UserDTO that was set to cause PersistenceException in mocked dao.
     * 
     * @return UserDTO
     */
    private UserDTO getExceptionUser() {
        User m4 = new User();
        m4.setId(4L);
        m4.setMail("User 4");
        return dozerBeanMapper.map(m4, UserDTO.class);
    }
}
