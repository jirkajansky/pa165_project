package cz.muni.fi.pa165.mushroomhunterportal.service.tests;

import cz.muni.fi.pa165.mushroomhunterportal.dao.ForestDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Forest;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.ForestService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.ForestDTO;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceException;
import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithSecurityContextTestExcecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;

/**
 * Tests for ForestService
 * 
 * @author Jan Bruzl
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:serviceApplicationContext.xml"})
@TestExecutionListeners(listeners={ServletTestExecutionListener.class,
		DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		WithSecurityContextTestExcecutionListener.class})
public class ForestServiceTest {

    @Autowired
    private ForestService forestService;

    @Mock
    private ForestDao dao;

    @Autowired 
    Mapper dozerBeanMapper;

    @Before
    public void setUp() {
        Forest f1 = new Forest();
        Forest f2 = new Forest();
        Forest f3 = new Forest();
        Forest f4 = new Forest();

        f1.setId(1);
        f2.setId(2);
        f3.setId(3);
        f4.setId(4);

        f1.setName("Forest 1");
        f2.setName("Forest 2");
        f3.setName("Forest 3)");
        f4.setName("Forest 4");

        f1.setDescription(null);
        f2.setDescription("Short desc");
        f3.setDescription("Short desc 2");
        f4.setDescription(null);
        
        f2.setLatitude(5);
        f2.setLongitude(5);
        
        f3.setLatitude(-5);
        f3.setLongitude(-5);

        List<Forest> forests = new ArrayList<>();
        forests.add(f1);
        forests.add(f2);
        forests.add(f3);
        forests.add(f4);

        dao = Mockito.mock(ForestDao.class);

        Mockito.when(dao.getAllForests()).thenReturn(forests);
        Mockito.when(dao.getById(eq(1L))).thenReturn(f1);
        Mockito.when(dao.getById(eq(2L))).thenReturn(f2);
        Mockito.when(dao.getById(eq(3L))).thenReturn(f3);
        Mockito.when(dao.getById(eq(4L))).thenReturn(f4);
        Mockito.when(dao.getById(not(or(or(eq(1L), eq(2L)), or(eq(3L), eq(4L)))))).thenReturn(null);
        
        Mockito.doThrow(new PersistenceException()).when(dao).deleteForest(f4);
        Mockito.doThrow(new PersistenceException()).when(dao).updateForest(f4);
        Mockito.doThrow(new PersistenceException()).when(dao).addForest(f4);       
        
        forestService.setDao(dao);
    }

    @Test
    public void basicTest() {
        Assert.assertNotNull(forestService);
        Assert.assertNotNull(dao);
        Assert.assertNotNull(dozerBeanMapper);
    }

    @Test(expected = DataAccessException.class)
    @WithMockUser(username="test",roles={"ADMINISTRATOR", "USER"})
    public void createTest() {
        ForestDTO forestDTO = new ForestDTO();
        forestDTO.setId(5);
        forestDTO.setName("Forest 5");
        forestDTO.setDescription("desc");

        Forest forestEntity = dozerBeanMapper.map(forestDTO, Forest.class);

        forestService.createForest(forestDTO);

        Mockito.verify(dao).addForest(forestEntity);
        
        ForestDTO exceptionForest = getExceptionForest();
        forestService.createForest(exceptionForest);
    }

    

    @Test(expected = DataAccessException.class)
    @WithMockUser(username="test",roles={"USER","ADMINISTRATOR"})
    public void updateTest() {
        ForestDTO forestDTO = new ForestDTO();
        forestDTO.setId(1);
        forestDTO.setName("Forest 1 updated");
        forestDTO.setDescription("Update desc.");

        Forest forestEntity = dozerBeanMapper.map(forestDTO, Forest.class);

        forestService.updateForest(forestDTO);

        Mockito.verify(dao).updateForest(forestEntity);
        
        ForestDTO exceptionForest = getExceptionForest();
        forestService.updateForest(exceptionForest);
    }

    @Test(expected = DataAccessException.class)
    @WithMockUser(username="test",roles={"USER","ADMINISTRATOR"})
    public void deleteTest() {
        ForestDTO forestDTO = new ForestDTO();
        forestDTO.setId(1);
        forestDTO.setName("Forest 1");
        forestDTO.setDescription(null);

        Forest forestEntity = dozerBeanMapper.map(forestDTO, Forest.class);

        forestService.deleteForest(forestDTO);

        Mockito.verify(dao).deleteForest(forestEntity);
        
        ForestDTO exceptionForest = getExceptionForest();
        forestService.deleteForest(exceptionForest);
    }
    
    @Test
    @WithMockUser(username="test",roles={"USER"})
    public void getByIdTest(){
        List<Forest> forests = dao.getAllForests();
        
        Assert.assertNotNull(forests);
        Assert.assertTrue(forests.size()>0);
        long existingId = forests.get(0).getId();
        
        
        ForestDTO forestDTO = forestService.getForestById(existingId);
        Assert.assertNotNull(forestDTO);
        Assert.assertEquals(existingId, forestDTO.getId());
        
        long nonExistingId = forests.size()+1;
        forestDTO = forestService.getForestById(nonExistingId);
        Assert.assertNull(forestDTO);
    }

    @Test
    @WithMockUser(username="test",roles={"USER"})
    public void getAllTest() {
        List<ForestDTO> forests = forestService.getAllForests();

        Mockito.verify(dao).getAllForests();

        Assert.assertNotNull("Returned list should never be null.", forests);

        final List<Forest> daoForests = dao.getAllForests();

        Assert.assertEquals("Expected list of size " + daoForests.size() + ", obtained list with size: " + forests.size(),
                daoForests.size(), forests.size());

        for (Forest forest : daoForests) {
            ForestDTO forestFromDao = dozerBeanMapper.map(forest, ForestDTO.class);
            Assert.assertTrue("Forest " + forest + " not found in service", forests.contains(forestFromDao));
        }

    }

    @Test
    @WithMockUser(username="test",roles={"USER"})
    public void findTest() {
        ForestDTO forestDTO = new ForestDTO();
        forestDTO.setId(1);
        forestDTO.setName("Forest 1");
        forestDTO.setDescription(null);

        List<ForestDTO> forests = forestService.findForest(forestDTO.getName());

        Mockito.verify(dao).getAllForests();

        Assert.assertNotNull("Returned list should never be null.", forests);

        Assert.assertEquals("Expected list of size 1, obtained list with size: " + forests.size(), 1, forests.size());

        Assert.assertEquals(forestDTO, forests.get(0));
        
        forests = forestService.findForest("Forest");

        Assert.assertNotNull("Returned list should never be null.", forests);

        Assert.assertEquals("Expected list of size 4, obtained list with size: " + forests.size(), 4, forests.size());
    }
    
    /**
     * Return ForestDTO that was set to cause PersistenceException in mocked dao.
     * 
     * @return ForestDTO
     */
    private ForestDTO getExceptionForest() {
        Forest f4 = new Forest();
        f4.setId(4);
        f4.setName("Forest 4");
        f4.setDescription(null);
        return dozerBeanMapper.map(f4, ForestDTO.class);
    }

}
