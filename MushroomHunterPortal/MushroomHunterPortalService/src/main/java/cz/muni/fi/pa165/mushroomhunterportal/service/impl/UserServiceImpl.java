package cz.muni.fi.pa165.mushroomhunterportal.service.impl;

import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.RoleDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.UserDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomHunterService;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.UserService;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.User;
import cz.muni.fi.pa165.mushroomhunterportal.dao.UserDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.logging.Level;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
@Transactional
@Service
@Configuration
public class UserServiceImpl implements UserService {

    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Value("${admin.default.mail}")
    private String adminLogin;

    @Value("${admin.default.password}")
    private String adminPassword;

    @Autowired
    private MushroomHunterService hunterService;

    @Autowired
    private UserDao dao;

    @Autowired
    Mapper dozerBeanMapper;

    @Override
    public void setDao(UserDao dao) {
        this.dao = dao;
    }

    @PostConstruct
    @Autowired
    public void checkDefaultAdmin() {
        if (!checkMailIsAlreadyTaken(adminLogin)) {
            try {
                SecurityContext ctx = SecurityContextHolder.createEmptyContext();
                SecurityContextHolder.setContext(ctx);
                List<GrantedAuthority> gaList = new ArrayList<>();
                gaList.add(new SimpleGrantedAuthority(RoleDTO.ROLE_ADMINISTRATOR.toString()));
                Authentication authentication = new UsernamePasswordAuthenticationToken(
                        "system",
                        "1234",
                        gaList
                );
                ctx.setAuthentication(authentication);

                UserDTO defaultAdmin = new UserDTO();
                defaultAdmin.setRole(RoleDTO.ROLE_ADMINISTRATOR);
                defaultAdmin.setMail(adminLogin);

                MushroomHunterDTO hunterDTO = new MushroomHunterDTO();
                hunterDTO.setFirstName("Admin");
                hunterDTO.setSurName("Admin");
                hunterService.createHunter(hunterDTO);

                defaultAdmin.setHunter(hunterDTO);
                createUser(defaultAdmin, adminPassword);

            } finally {
                SecurityContextHolder.clearContext();
            }

        }
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public void updateUser(UserDTO user) {
        org.springframework.security.core.userdetails.User loggedUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean isAdmin = false;
        for (GrantedAuthority ga : loggedUser.getAuthorities()) {
            if (ga.getAuthority().equals(RoleDTO.ROLE_ADMINISTRATOR.toString())) {
                isAdmin = true;
            }
        }
        if (!isAdmin) {
            if (!loggedUser.getUsername().equals(user.getMail())) {
                logger.error("Failed to authenticate user {} to change password of user {}", loggedUser.getUsername(), user.getMail());
                throw new AccessDeniedException("Access denied for password change");
            }
        }

        if (user == null) {
            logger.error("updateUser: illegal argument supplied - null");
            throw new IllegalArgumentException("updateUser: Supplied null paramenter.");
        }

        User userEntity = dozerBeanMapper.map(user, User.class);

        try {
            userEntity = dao.save(userEntity);
        } catch (Exception ex) {
            logger.error("Exception while updating User {} : {}", userEntity, ex.getMessage());
            throw new DataAccessException("Cannot update user " + user) {
            };
        }
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ADMINISTRATOR')")
    public void deleteUser(UserDTO user) {
        if (user == null) {
            logger.error("deleteUser: illegal argument supplied - null");
            throw new IllegalArgumentException("deleteUser: Supplied null paramenter.");
        }
        User userEntity = dozerBeanMapper.map(user, User.class);

        try {
            dao.delete(userEntity);
        } catch (Exception ex) {
            logger.error("Exception while deleting User {} : {}", userEntity, ex.getMessage());
            throw new DataAccessException("Exception while deleting User " + user) {
            };
        }

    }

    @Override
    @PreAuthorize("isAnonymous() or hasAuthority('ROLE_ADMINISTRATOR')")
    public void createUser(UserDTO user, String password) {
        if (user == null) {
            logger.error("createUser: illegal argument supplied - null");
            throw new IllegalArgumentException("createUser: Supplied null paramenter.");
        }
        User userEntity = dozerBeanMapper.map(user, User.class);
        logger.info("creating User, dao instance: " + dao.toString());

        try {
            userEntity.setPassword(password);
            userEntity = dao.save(userEntity);
            user.setId(userEntity.getId());
        } catch (Exception ex) {
            logger.error("Exception while creating User {} : {}", userEntity, ex.getMessage());
            throw new DataAccessException("Cannot create user " + user) {
            };
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public UserDTO getById(Long id) {
        User userEntity = null;
        try {
            List<UserDTO> users =  getAll();
            for (UserDTO u : users)
            {
                if (u.getId() == id)
                    return u;
            }
            
        } catch (Exception ex) {
            logger.error("Exception while getting User by id {} : {}", id, ex.getMessage());
            throw new DataAccessException("Exception when getting user with id " + id) {
            };
        }
        if (userEntity == null) {
            return null;
        }
        UserDTO userDTO = dozerBeanMapper.map(userEntity, UserDTO.class);

        return userDTO;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public UserDTO getByMail(String mail) {

        User userEntity = dao.getByMail(mail);
        if (userEntity == null) {
            return null;
        }
        UserDTO userDTO = dozerBeanMapper.map(userEntity, UserDTO.class);
        return userDTO;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    @Override
    public UserDTO getByHunter(MushroomHunterDTO hunter) {

        MushroomHunter hunterEntity = dozerBeanMapper.map(hunter, MushroomHunter.class);

        User user = dao.getByHunter(hunterEntity);

        UserDTO userDTO = dozerBeanMapper.map(user, UserDTO.class);
        return userDTO;
    }

    @Override
    public UserDTO getLogedUser() {
        org.springframework.security.core.userdetails.User suser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Iterable<User> userEntities = dao.findAll();
        if (userEntities == null) {
            return null;
        }
        for (User u : userEntities) {
            if (u.getHunter() != null && u.getMail().contains(suser.getUsername())) {
                return dozerBeanMapper.map(u, UserDTO.class);
            }
        }
        return null;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    public List<UserDTO> getAll() {
        Iterable<User> userEntities = dao.findAll();
        List<UserDTO> usersDTOList = new ArrayList<>();
        if (userEntities == null) {
            return usersDTOList;
        }
        for (User m : userEntities) {
            usersDTOList.add(dozerBeanMapper.map(m, UserDTO.class));
        }
        return usersDTOList;
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public void setPassword(UserDTO user, String password) {

        org.springframework.security.core.userdetails.User loggedUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean isAdmin = false;
        for (GrantedAuthority ga : loggedUser.getAuthorities()) {
            if (ga.getAuthority().equals(RoleDTO.ROLE_ADMINISTRATOR)) {
                isAdmin = true;
            }
        }
        if (!isAdmin) {
            if (!loggedUser.getUsername().equals(user.getMail())) {
                logger.error("Failed to authenticate user {} to change password of user {}", loggedUser.getUsername(), user.getMail());
                throw new AccessDeniedException("Access denied for password change");
            }
        }

        if (user == null) {
            logger.error("updateUser: illegal argument supplied - null");
            throw new IllegalArgumentException("updateUser: Supplied null paramenter.");
        }

        User userEntity = dozerBeanMapper.map(user, User.class);
        try {
            userEntity.setPassword(password);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            userEntity = dao.save(userEntity);
        } catch (Exception ex) {
            logger.error("Exception while updating User {} : {}", userEntity, ex.getMessage());
            throw new DataAccessException("Cannot update user " + user) {
            };
        }

    }

    @Override
    public boolean checkMailIsAlreadyTaken(String mail) {
        return dao.getByMail(mail) != null;
    }

}
