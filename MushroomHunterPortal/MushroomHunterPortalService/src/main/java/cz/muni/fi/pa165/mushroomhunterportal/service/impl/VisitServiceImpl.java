package cz.muni.fi.pa165.mushroomhunterportal.service.impl;

import cz.muni.fi.pa165.mushroomhunterportal.dao.VisitDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Forest;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Visit;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.VisitService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.ForestDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.RoleDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.UserDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.VisitDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.UserService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jan Trnka
 */
@Transactional
@Component
public class VisitServiceImpl implements VisitService {

    private final Logger logger = LoggerFactory.getLogger(VisitServiceImpl.class);
    @Autowired
    private VisitDao dao;
    @Autowired
    Mapper dozerBeanMapper;
    
    @Autowired
    private UserService userService;

    @Override
    public void setDao(VisitDao dao) {
        this.dao = dao;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public void createVisit(VisitDTO visit) {
        org.springframework.security.core.userdetails.User loggedUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean isAdmin = false;
        for(GrantedAuthority ga : loggedUser.getAuthorities()){
            if(ga.getAuthority().equals(RoleDTO.ROLE_ADMINISTRATOR.toString())){
                isAdmin = true;
            }
        }
        if(!isAdmin){
            UserDTO user = userService.getByHunter(visit.getHunter());
            if(!loggedUser.getUsername().equals(user.getMail())){
                logger.error("Failed to authenticate user {} to change password of user {}", loggedUser.getUsername(), user.getMail());
                throw new AccessDeniedException("Access denied for password change");
            }
        }
        if (visit == null) {
            logger.error("createVisit: illegal argument supplied - null");
            throw new IllegalArgumentException("createVisit: Supplied null parameter.");
        } else if (visit.getForest() == null || visit.getHunter() == null) {
            throw new IllegalArgumentException("Cannot save visit that doesn't have forest or hunter relation set.");
        } else {
            Visit visitEntity = dozerBeanMapper.map(visit, Visit.class);
            try {
                dao.addVisit(visitEntity);
                visit.setId(visitEntity.getId());
            } catch (Exception ex) {
                logger.error("Exception while creating visit {} : {}", visitEntity, ex.getMessage());
                throw new DataAccessException("Cannot create visit " + visit) {
                };

            }
        }

    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public void deleteVisit(VisitDTO visit) {
        org.springframework.security.core.userdetails.User loggedUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean isAdmin = false;
        for(GrantedAuthority ga : loggedUser.getAuthorities()){
            if(ga.getAuthority().equals(RoleDTO.ROLE_ADMINISTRATOR.toString())){
                isAdmin = true;
            }
        }
        if(!isAdmin){
            UserDTO user = userService.getByHunter(visit.getHunter());
            if(!loggedUser.getUsername().equals(user.getMail())){
                logger.error("Failed to authenticate user {} to change password of user {}", loggedUser.getUsername(), user.getMail());
                throw new AccessDeniedException("Cannot delete visit that belongs to somebody else.");
            }
        }
        if (visit == null) {
            logger.error("deleteVisit: illegal argument supplied - null");
            throw new IllegalArgumentException("deleteVisit: Supplied null parameter.");
        } else if (visit.getForest() == null || visit.getHunter() == null) {
            throw new IllegalArgumentException("Cannot delete visit that doesn't have forest or hunter relation set.");
        } else {
            Visit visitEntity = dozerBeanMapper.map(visit, Visit.class);
            try {
                dao.deleteVisit(visitEntity);
            } catch (Exception ex) {
                logger.error("Exception while deleting visit {} : {}", visitEntity, ex.getMessage());
                throw new DataAccessException("Cannot delete visit: " + visit) {
                };

            }
        }

    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public void updateVisit(VisitDTO visit) {
        org.springframework.security.core.userdetails.User loggedUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean isAdmin = false;
        for(GrantedAuthority ga : loggedUser.getAuthorities()){
            if(ga.getAuthority().equals(RoleDTO.ROLE_ADMINISTRATOR.toString())){
                isAdmin = true;
            }
        }
        if(!isAdmin){
            UserDTO user = userService.getByHunter(visit.getHunter());
            if(!loggedUser.getUsername().equals(user.getMail())){
                logger.error("Failed to authenticate user {} to change password of user {}", loggedUser.getUsername(), user.getMail());
                throw new AccessDeniedException("Access denied for password change");
            }
        }
        if (visit == null) {
            logger.error("updateVisit: illegal argument supplied - null");
            throw new IllegalArgumentException("updateVisit: Supplied null parameter.");
        } else if (visit.getForest() == null || visit.getHunter() == null) {
            throw new IllegalArgumentException("Cannot update visit that doesn't have forest or hunter relation set.");
        } else {

            Visit visitEntity = dozerBeanMapper.map(visit, Visit.class);
            try {
                dao.updateVisit(visitEntity);
            } catch (Exception ex) {
                logger.error("Exception while updating visit {} : {}", visitEntity, ex.getMessage());
                throw new DataAccessException("Cannot update visit " + visit) {
                };
            }
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public VisitDTO getVisitById(long id) {
        Visit visitEntity = null;
        try {
            visitEntity = dao.getById(id);
        } catch (Exception ex) {
            logger.error("Exception while getting visit by id {} : {}", id, ex.getMessage());
            throw new DataAccessException("Exception when getting visit with id " + id) {
            };
        }
        if (visitEntity == null) {
            return null;
        } else {
            VisitDTO visitDTO = dozerBeanMapper.map(visitEntity, VisitDTO.class);
            return visitDTO;
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<VisitDTO> getAllVisits() {
        List<Visit> visitEntities = dao.getAllVisit();
        List<VisitDTO> visitDTOs = new ArrayList<>();
        if (visitEntities != null) {
            visitDTOs = this.transformVisitEntitiesToDTOs(visitEntities);
        }
        return visitDTOs;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<VisitDTO> getVisitsByForest(ForestDTO forest) {
        return findVisit(null, null, forest, null);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<VisitDTO> getVisitsByHunter(MushroomHunterDTO hunter) {
        return findVisit(null, null, null, hunter);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<VisitDTO> findVisit(Date afterDate, Date beforeDate, ForestDTO forest, MushroomHunterDTO mushroomHunter) {
        List<Visit> visitEntities = dao.getAllVisit();
        List<Visit> filteredEntities = new ArrayList<>();
        List<VisitDTO> visitDTOs = null;
        Boolean aDate = (afterDate != null);
        Boolean bDate = (beforeDate != null);
        Boolean f = (forest != null);
        Boolean mh = (mushroomHunter != null);

        if (!aDate && !bDate && !f && !mh) {                            
            return this.transformVisitEntitiesToDTOs(visitEntities);
        }
        if (visitEntities != null) {
            Boolean aDateOk;
            Boolean bDateOk;
            Boolean fOk;
            Boolean mhOk;
            Forest forestEntity = null;
            MushroomHunter mushroomHunterEntity = null;
            if (f) {
                forestEntity = dozerBeanMapper.map(forest, Forest.class);
            }
            if (mh) {
                mushroomHunterEntity = dozerBeanMapper.map(mushroomHunter, MushroomHunter.class);
            }

            for (Visit v : visitEntities) {

                aDateOk = !aDate || (aDate && (v.getDateOfVisit().compareTo(afterDate) >= 0));
                bDateOk = !bDate || (bDate && (v.getDateOfVisit().compareTo(beforeDate) <= 0));
                fOk = !f || (f && (forestEntity.equals(v.getForest())));
                mhOk = !mh || (mh && mushroomHunterEntity.equals(v.getHunter()));

                if (aDateOk && bDateOk && fOk && mhOk) {
                    filteredEntities.add(v);
                }
            }
        }
        visitDTOs = transformVisitEntitiesToDTOs(filteredEntities);
        return visitDTOs;

    }

    private List<VisitDTO> transformVisitEntitiesToDTOs(List<Visit> visitEntities) {
        List<VisitDTO> visitDTOs = new ArrayList<>();
        if (visitEntities != null) {
            for (Visit v : visitEntities) {
                visitDTOs.add(dozerBeanMapper.map(v, VisitDTO.class));
            }
        }
        return visitDTOs;
    }

}
