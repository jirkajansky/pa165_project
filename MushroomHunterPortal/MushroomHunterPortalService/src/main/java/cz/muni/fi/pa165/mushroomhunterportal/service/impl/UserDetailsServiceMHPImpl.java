package cz.muni.fi.pa165.mushroomhunterportal.service.impl;


import cz.muni.fi.pa165.mushroomhunterportal.dao.UserDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Role;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.UserDetailsServiceMHP;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Mushroom Hunter Portal implementation of {@link UserDetailsService}
 *
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
@Service("userDetailsService")
@Transactional(readOnly = true)
public class UserDetailsServiceMHPImpl implements UserDetailsServiceMHP {

    @Autowired
    private UserDao userDao;

    @Override
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }        

    @Override
    public UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {

        cz.muni.fi.pa165.mushroomhunterportal.dao.entities.User user = userDao.getByMail(username);
        List<GrantedAuthority> authorities
                = buildUserAuthority(user.getRole());

        return buildUserForAuthentication(user, authorities);

    }
    


    /**
     * Converts {@link cz.muni.fi.pa165.mushroomhunterportal.dao.entities.User} to {@link User}
     *
     * @param user {@link cz.muni.fi.pa165.mushroomhunterportal.dao.entities.User}
     * @param authorities List of {@link GrantedAuthority}
     * @return
     */
    private User buildUserForAuthentication(cz.muni.fi.pa165.mushroomhunterportal.dao.entities.User user, List<GrantedAuthority> authorities) {
        return new User(user.getMail(), user.getPassword(),
                true, true, true, true, authorities);
    }

    /**
     * Build list of authorities from roles.
     *
     * @param userRoles {@link Role}
     * @return List of {@link GrantedAuthority}
     */
    private List<GrantedAuthority> buildUserAuthority(Role role) {
        Set<GrantedAuthority> setAuths = new HashSet<>();

        setAuths.add(new SimpleGrantedAuthority(role.toString()));

        List<GrantedAuthority> result = new ArrayList<>(setAuths);
        return result;
    }
}
