package cz.muni.fi.pa165.mushroomhunterportal.service.impl;

import cz.muni.fi.pa165.mushroomhunterportal.dao.MushroomDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Mushroom;

import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomTypeDTO;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * MushroomAPI implementation
 *
 * @author Jan Bruzl
 */
@Transactional
@Component
public class MushroomServiceImpl implements MushroomService {

    private final Logger logger = LoggerFactory.getLogger(MushroomServiceImpl.class);

    @Autowired
    private MushroomDao mushroomDao;

    @Autowired
    Mapper dozerBeanMapper;

    public void setMushroomDao(MushroomDao mushroomDao) {
        logger.info("Set mushroomDao: " + mushroomDao.toString());
        this.mushroomDao = mushroomDao;
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ADMINISTRATOR')")
    public void updateMushroom(MushroomDTO mushroom) {
        if (mushroom == null) {
            logger.error("updateMushroom: illegal argument supplied - null");
            throw new IllegalArgumentException("updateMushroom: Supplied null paramenter.");            
        }

        Mushroom mushroomEntity = dozerBeanMapper.map(mushroom, Mushroom.class);

        try {
            mushroomDao.updateMushroom(mushroomEntity);
        } catch (Exception ex) {
            logger.error("Exception while updating Mushroom {} : {}", mushroomEntity, ex.getMessage());
            throw new DataAccessException("Cannot update mushroom " + mushroom) {
            };
        }
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ADMINISTRATOR')")
    public void deleteMushroom(MushroomDTO mushroom) {
        if (mushroom == null) {
            logger.error("deleteMushroom: illegal argument supplied - null");
            throw new IllegalArgumentException("deleteMushroom: Supplied null paramenter.");
        }
        Mushroom mushroomEntity = dozerBeanMapper.map(mushroom, Mushroom.class);

        try {
            mushroomDao.deleteMushroom(mushroomEntity);
        } catch (Exception ex) {
            logger.error("Exception while deleting Mushroom {} : {}", mushroomEntity, ex.getMessage());
            throw new DataAccessException("Exception while deleting Mushroom " + mushroom) {
            };
        }

    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ADMINISTRATOR')")
    public void createMushroom(MushroomDTO mushroom) {
        if (mushroom == null) {
            logger.error("createMushroom: illegal argument supplied - null");
            throw new IllegalArgumentException("createMushroom: Supplied null paramenter.");
        }
        Mushroom mushroomEntity = dozerBeanMapper.map(mushroom, Mushroom.class);
            logger.error("creating Mushroom, mushroomDao instance: " + mushroomDao.toString());

        try {
            mushroomDao.addMushroom(mushroomEntity);
            mushroom.setId(mushroomEntity.getId());
        } catch (Exception ex) {
            logger.error("Exception while creating Mushroom {} : {}", mushroomEntity, ex.getMessage());
            throw new DataAccessException("Cannot create mushroom " + mushroom) {
            };
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public MushroomDTO getMushroomById(long id) {
        Mushroom mushroomEntity = null;
        try {
            mushroomEntity = mushroomDao.getById(id);
        } catch (Exception ex) {
            logger.error("Exception while getting Mushroom by id {} : {}", id, ex.getMessage());
            throw new DataAccessException("Exception when getting mushroom with id " + id) {
            };
        }
        if (mushroomEntity == null) {
            return null;
        }
        MushroomDTO mushroomDTO = dozerBeanMapper.map(mushroomEntity, MushroomDTO.class);

        return mushroomDTO;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<MushroomDTO> getAllMushrooms() {
        List<MushroomDTO> mushroomDTOList = new ArrayList<>();
        List<Mushroom> mushroomEntities = mushroomDao.getAllMushrooms();

        if (mushroomEntities == null) {
            return mushroomDTOList;
        }
        for (Mushroom m : mushroomEntities) {
            mushroomDTOList.add(dozerBeanMapper.map(m, MushroomDTO.class));
        }

        return mushroomDTOList;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<MushroomDTO> getEdibleMushrooms() {
        return getMushroomByType(MushroomTypeDTO.EDIBLE);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<MushroomDTO> getUnedibleMushrooms() {
        return getMushroomByType(MushroomTypeDTO.UNEDIBLE);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<MushroomDTO> getPoisonousMushrooms() {
        return getMushroomByType(MushroomTypeDTO.POISONOUS);
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<MushroomDTO> findMushroom(String name) {
        List<MushroomDTO> mushroomDTOList = getAllMushrooms();
        List<MushroomDTO> mushroomsByName = new ArrayList<>();

        for (MushroomDTO mushroomDTO : mushroomDTOList) {
            if (mushroomDTO.getName().contains(name)) {
                mushroomsByName.add(mushroomDTO);
            }
        }

        return mushroomsByName;
    }

    /**
     * Returns mushrooms by given type
     *
     * @param type
     * @return
     */
    private List<MushroomDTO> getMushroomByType(MushroomTypeDTO type) {
        List<MushroomDTO> mushroomDTOList = getAllMushrooms();
        List<MushroomDTO> mushroomsByType = new ArrayList<>();

        for (MushroomDTO mushroomDTO : mushroomDTOList) {
            if (mushroomDTO.getType() == type) {
                mushroomsByType.add(mushroomDTO);
            }
        }

        return mushroomsByType;
    }

    @Override
    public void setDao(MushroomDao dao) {
        if(dao == null){
            logger.error("setDao: illegal argument supplied - null");
            throw new IllegalArgumentException("setDao: Supplied null paramenter.");
        }
        mushroomDao = dao;
    }
}
