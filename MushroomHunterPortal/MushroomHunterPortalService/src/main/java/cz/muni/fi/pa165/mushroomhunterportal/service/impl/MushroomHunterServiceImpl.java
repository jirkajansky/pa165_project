package cz.muni.fi.pa165.mushroomhunterportal.service.impl;

import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import cz.muni.fi.pa165.mushroomhunterportal.dao.MushroomHunterDao;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomHunterService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.RoleDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.UserDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.UserService;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jiří Jánský
 */
@Transactional
@Component
public class MushroomHunterServiceImpl implements MushroomHunterService {

    private final Logger logger = LoggerFactory.getLogger(MushroomServiceImpl.class);

    @Autowired
    private MushroomHunterDao mushroomHunterDao;
    @Autowired
    private Mapper dozerBeanMapper;
    @Autowired
    private UserService userService;

    @Override
    public void setMushroomHunterDao(MushroomHunterDao dao) {
        if (dao == null) {
            logger.error("setMushroomHunterDao: illegal argument supplied - null");
            throw new IllegalArgumentException("setMushroomHunterDao: Supplied null paramenter.");
        }
        this.mushroomHunterDao = dao;
    }

    @Override
    public void createHunter(MushroomHunterDTO hunter) {
        if (hunter == null) {
            logger.error("createHunter: illegal argument supplied - null");
            throw new IllegalArgumentException("createHunter: Supplied null paramenter.");
        }
        MushroomHunter hunterEntity = dozerBeanMapper.map(hunter, MushroomHunter.class);
        try {
            mushroomHunterDao.addHunter(hunterEntity);
            hunter.setId(hunterEntity.getId());
        } catch (Exception ex) {
            logger.error("Exception while creating MushroomHunter {} : {}", hunterEntity, ex.getMessage());
            throw new DataAccessException("Cannot create MushroomHunter " + hunter) {
            };
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public void updateHunter(MushroomHunterDTO hunter) {
        org.springframework.security.core.userdetails.User loggedUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean isAdmin = false;
        for(GrantedAuthority ga : loggedUser.getAuthorities()){
            if(ga.getAuthority().equals(RoleDTO.ROLE_ADMINISTRATOR.toString())){
                isAdmin = true;
            }
        } 
        if(!isAdmin){
            UserDTO user = userService.getByHunter(hunter);
            if(!loggedUser.getUsername().equals(user.getMail())){
                logger.error("Failed to authenticate user {} to change password of user {}", loggedUser.getUsername(), user.getMail());
                throw new AccessDeniedException("Access denied for password change");
            }
        }
        if (hunter == null) {
            logger.error("updateHunter: illegal argument supplied - null");
            throw new IllegalArgumentException("updateHunter: Supplied null paramenter.");
        }
        MushroomHunter hunterEntity = dozerBeanMapper.map(hunter, MushroomHunter.class);
        try {
            mushroomHunterDao.updateHunter(hunterEntity);
        } catch (Exception ex) {
            logger.error("Exception while updating MushroomHunter {} : {}", hunterEntity, ex.getMessage());
            throw new DataAccessException("Cannot create MushroomHunter " + hunter) {
            };
        }
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ADMINISTRATOR')")
    public void deleteHunter(MushroomHunterDTO hunter) {
        if (hunter == null) {
            logger.error("deleteHunter: illegal argument supplied - null");
            throw new IllegalArgumentException("deleteHunter: Supplied null paramenter.");
        }
        MushroomHunter hunterEntity = dozerBeanMapper.map(hunter, MushroomHunter.class);
        try {
            mushroomHunterDao.deleteHunter(hunterEntity);
        } catch (Exception ex) {
            logger.error("Exception while deleting MushroomHunter {} : {}", hunterEntity, ex.getMessage());
            throw new DataAccessException("Cannot create MushroomHunter " + hunter) {
            };
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public MushroomHunterDTO getById(long id) {
        MushroomHunter hunterEntity = null;
        try {
            hunterEntity = mushroomHunterDao.getById(id);
        } catch (Exception ex) {
            logger.error("Exception while getting MushroomHunter by id {} : {}", id, ex.getMessage());
            throw new DataAccessException("Cannot create MushroomHunter " + hunterEntity) {
            };
        }
        return (hunterEntity != null) ? dozerBeanMapper.map(hunterEntity, MushroomHunterDTO.class): null;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<MushroomHunterDTO> getAllHunters() {
        List<MushroomHunterDTO> mushroomHunterDTOs = new ArrayList<>();
        List<MushroomHunter> mushroomHunterEntities = mushroomHunterDao.getAllHunters();
        if (mushroomHunterEntities == null) {
            return mushroomHunterDTOs;
        }
        for (MushroomHunter m : mushroomHunterEntities) {
            mushroomHunterDTOs.add(dozerBeanMapper.map(m, MushroomHunterDTO.class));
        }
        return mushroomHunterDTOs;
    }

    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<MushroomHunterDTO> findHunter(String search) {

        List<MushroomHunterDTO> hunterDTOs = getAllHunters();
        List<MushroomHunterDTO> searchedHunter = new ArrayList<>();

        for (MushroomHunterDTO hunterDTO : hunterDTOs) {
            if (hunterDTO.toString().contains(search)) {
                searchedHunter.add(hunterDTO);
            }
        }
        return searchedHunter;
    }

}
