package cz.muni.fi.pa165.mushroomhunterportal.service.impl;

import cz.muni.fi.pa165.mushroomhunterportal.dao.ForestDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Forest;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.ForestService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.ForestDTO;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for Forest
 * @author Martina Prochazkova
 */
@Transactional
@Component
public class ForestServiceImpl implements ForestService {
    
    private final Logger logger = LoggerFactory.getLogger(MushroomServiceImpl.class);
    
    @Autowired
    private ForestDao forestDao;
    @Autowired 
    Mapper dozerBeanMapper;

    /**
     * udpatig forest
     * @param forest 
     */
    @Override
    @PreAuthorize("hasAuthority('ROLE_ADMINISTRATOR')")
    public void updateForest(ForestDTO forest) {
        if (forest == null) {
            logger.error("updateForest: illegal argument supplied - null");
            throw new IllegalArgumentException("updateForest: Supplied null paramenter.");            
        }
        
        Forest forestEntity = dozerBeanMapper.map(forest, Forest.class);
        
        try {
            forestDao.updateForest(forestEntity);
        } catch (Exception ex) {
            logger.error("Exception while updating Forest {} : {}", forestEntity, ex.getMessage());
            throw new DataAccessException("Cannot update Forest " + forest) {
            };
        }
    }

    /**
     * delete forest
     * @param forest 
     */
    @Override
    @PreAuthorize("hasAuthority('ROLE_ADMINISTRATOR')")
    public void deleteForest(ForestDTO forest) {
        if (forest == null) {
            logger.error("deleteForest: illegal argument supplied - null");
            throw new IllegalArgumentException("deleteForest: Supplied null paramenter.");            
        }
        
        Forest forestEntity = dozerBeanMapper.map(forest, Forest.class);

        try {
            forestDao.deleteForest(forestEntity);
        } catch (Exception ex) {
            logger.error("Exception while deleting Foret {} : {}", forestEntity, ex.getMessage());
            throw new DataAccessException("Exception while deleting Forest " + forest) {
            };
        }
    }

    /**
     * create forest
     * @param forest 
     */
    @Override
    @PreAuthorize("hasAuthority('ROLE_ADMINISTRATOR')")
    public void createForest(ForestDTO forest) {
        if (forest == null) {
            logger.error("createForest: illegal argument supplied - null");
            throw new IllegalArgumentException("createForest: Supplied null paramenter.");            
        }
        
        Forest forestEntity = dozerBeanMapper.map(forest, Forest.class);

        try {
            forestDao.addForest(forestEntity);
            forest.setId(forestEntity.getId());
        } catch (Exception ex) {
            logger.error("Exception while creating Forest {} : {}", forestEntity, ex.getMessage());
            throw new DataAccessException("Cannot create Forest " + forest) {
            };
        }
    }
    
    /**
     * 
     * @param id
     * @return forest by id
     */
    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public ForestDTO getForestById(long id) {
        Forest forestEntity = null;
        try {
            forestEntity = forestDao.getById(id);
        } catch (Exception ex) {
            logger.error("Exception while getting Forest by id {} : {}", id, ex.getMessage());
            throw new DataAccessException("Exception when getting forest with id " + id) {
            };
        }
        if (forestEntity == null)
            return null;
        ForestDTO forestDTO = dozerBeanMapper.map(forestEntity, ForestDTO.class);

        return forestDTO;
    }

    /**
     * 
     * @return all forest
     */
    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<ForestDTO> getAllForests() {
        List<ForestDTO> forestDTOList = new ArrayList<>();
                
        List<Forest> forestEntities = forestDao.getAllForests();
        if (forestEntities == null) {
            return forestDTOList;
        }
        for (Forest f : forestEntities)        
        {
            forestDTOList.add(dozerBeanMapper.map(f, ForestDTO.class));
        }
        
        return forestDTOList;
    }

    /**
     * 
     * @param name
     * @return findet forest by name 
     */
    @Override
    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_USER')")
    public List<ForestDTO> findForest(String name) {
        List<ForestDTO> forestDTOList = getAllForests();
        List<ForestDTO> forestsByName = new ArrayList<>();

        for (ForestDTO forestDTO : forestDTOList) {
            if (forestDTO.getName().contains(name)) {
                forestsByName.add(forestDTO);
            }
        }
        return forestsByName;
    }

    /**
     * set dao for testing with mock 
     * @param dao 
     */
    @Override
    public void setDao(ForestDao dao) {
        if(dao == null){
            logger.error("setDao: illegal argument supplied - null");
            throw new IllegalArgumentException("setDao: Supplied null paramenter.");
        }
        forestDao = dao;
    }
}
