package cz.muni.fi.pa165.mushroomhunterportal.tests;

import cz.muni.fi.pa165.mushroomhunterportal.dao.MushroomDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Mushroom;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import static org.junit.Assert.fail;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Jiri Jansky
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:applicationContext.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class MushroomDaoImplTest {
    
    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    private MushroomDao mushroomDao;
    
    public MushroomDaoImplTest() {
    }
    
    /**
     * test add mushroom
     */
    @Test
    public void mushroomAdd() 
    {
           
           Mushroom mush = new Mushroom();
           mush.setName("M");           
           mushroomDao.addMushroom(mush);

           Mushroom mDB = em.createQuery("Select m From Mushroom m", Mushroom.class).getSingleResult();

           Assert.assertEquals(mush, mDB);           

    }
    
    /**
     * test delete mushroom 
     */
    @Test
    public void mushroomDelete() 
    {
           Mushroom mush = new Mushroom();
           mush.setName("M");
           
           mushroomDao.addMushroom(mush);

           Mushroom mDB = em.createQuery("Select m From Mushroom m", Mushroom.class).getSingleResult();

           Assert.assertEquals(mush, mDB);

           mushroomDao.deleteMushroom(mush);
           assertMushroomExists();


    }
    
    /**
     * test update mushroom
     */
    @Test
    public void mushroomUpdate()
    {
        Mushroom mush = new Mushroom();
        mush.setName("L");
        mushroomDao.addMushroom(mush);
        mush.setName("K");
        mushroomDao.updateMushroom(mush);
        Mushroom  mDB = mushroomDao.getById(mush.getId());
        Assert.assertEquals("K", mDB.getName());
    }
    
    /**
     * test get mushroom by id
     */
    @Test
    public void mushroomGetById()
    {
           Mushroom mush = new Mushroom();
           mush.setName("L");          
           mushroomDao.addMushroom(mush);
           Mushroom  mDB = mushroomDao.getById(mush.getId());
           Assert.assertEquals(mush, mDB);

    }
    
    /**
     * test collection of mushroom
     */
    @Test
    public void mushroomCollection()
    {
        Mushroom mush1 = new Mushroom();
        Mushroom mush2 = new Mushroom();
        Mushroom mush3 = new Mushroom();
        Mushroom mush4 = new Mushroom();
        mush1.setName("A");
        mush2.setName("B");
        mush3.setName("C");
        mush4.setName("D");
        mushroomDao.addMushroom(mush1);
        mushroomDao.addMushroom(mush2);
        mushroomDao.addMushroom(mush3);
        mushroomDao.addMushroom(mush4);
        
        Set<Mushroom> mushrooms = new HashSet<Mushroom>();
        mushrooms.add(mush1);
        mushrooms.add(mush2);
        mushrooms.add(mush3);
        mushrooms.add(mush4);
        
        List<Mushroom> mushroomsDB = mushroomDao.getAllMushrooms();
        
        for(Mushroom item : mushrooms)
        {
            Assert.assertTrue(mushroomsDB.contains(item));
        }
    }
     
    /**
     * helpfull function for test
     */
    private void assertMushroomExists() {
            Boolean exists = (Boolean)em.createQuery("SELECT COUNT(m) > 0 FROM Mushroom m").getSingleResult();

            Assert.assertFalse(exists);
    }
    
    /**
     * test for bad input dao
     */
    @Test
    public void badInputsTest(){    
        List<Mushroom> hunters = null;
        
        try{
            hunters = mushroomDao.getAllMushrooms();
        }catch(Exception ex){
            fail("getAll on empty database shouldn't throw exception. Exception thrown: " + ex);
        }
        Assert.assertNotNull("getAll method should return empty list for empty database, not null", hunters);
        
        Mushroom m1 = null;
        try{
            m1 = mushroomDao.getById(-1L);
        }catch(Exception ex){
            fail("getById method houldn't throw exception when requesting non-existing id. Thrown: " + ex);
        }
        Assert.assertNull("getById should return null when requesting non-existing id", m1);
        
        Mushroom m2 = new Mushroom();
        try{
            mushroomDao.deleteMushroom(m2);            
        }catch(Exception ex){
            fail("No exception should be thrown when deleting non-persistent entity");
        }
    }
     
     
}
