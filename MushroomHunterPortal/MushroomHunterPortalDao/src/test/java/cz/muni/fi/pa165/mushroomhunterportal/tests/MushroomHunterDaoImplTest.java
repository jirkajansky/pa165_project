package cz.muni.fi.pa165.mushroomhunterportal.tests;

import cz.muni.fi.pa165.mushroomhunterportal.dao.MushroomHunterDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Forest;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Visit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import org.junit.Assert;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

/**
 *
 * @author Jan Bruzl
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:applicationContext.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class MushroomHunterDaoImplTest {  
    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    private MushroomHunterDao mushroomHunterDao;
        
    @Test    
    public void basicCreateDeleteTest(){
        MushroomHunter mh1 = new MushroomHunter();

        mh1.setFirstName("Karl");
        mh1.setSurName("Franz");
        mushroomHunterDao.addHunter(mh1);
        final long mhId = mh1.getId();
        
        MushroomHunter mhDB = em.find(MushroomHunter.class, mhId);
        Assert.assertEquals(mh1, mhDB);
        
        MushroomHunter mhFromDao = mushroomHunterDao.getById(mhDB.getId());
        
        Assert.assertEquals(mh1, mhFromDao); 
        
        try{
            mushroomHunterDao.deleteHunter(mh1);
        }catch(Exception ex){
            fail("No exception should be thrown: " + ex);
        }
        
        
        Assert.assertNull(em.find(MushroomHunter.class, mhId));
       
    }
    
    @Test
    public void badInputsTest(){    
        List<MushroomHunter> hunters = null;
        
        try{
            hunters = mushroomHunterDao.getAllHunters();
        }catch(Exception ex){
            fail("getAll on empty database shouldn't throw exception. Exception thrown: " + ex);
        }
        Assert.assertNotNull("getAll method should return empty list for empty database, not null", hunters);
        
        MushroomHunter mh1 = null;
        try{
            mh1 = mushroomHunterDao.getById(-1L);
        }catch(Exception ex){
            fail("getById method houldn't throw exception when requesting non-existing id. Thrown: " + ex);
        }
        Assert.assertNull("getById should return null when requesting non-existing id", mh1);
        
        MushroomHunter mh2 = new MushroomHunter();
        mh2.setFirstName("Karl2");
        mh2.setSurName("Franz2");
        try{
            mushroomHunterDao.deleteHunter(mh2);            
        }catch(Exception ex){
            fail("No exception should be thrown when deleting non-persistent entity");
        }
    }
    
    @Test
    public void getAllTest(){        
        MushroomHunter mh1 = new MushroomHunter();
        MushroomHunter mh2 = new MushroomHunter();
        MushroomHunter mh3 = new MushroomHunter();
        
        Set<Visit> visits = new HashSet<>();
        
        mh1.setFirstName("Karl");
        mh1.setSurName("Franz");
        mh1.setVisits(visits);
        
        mh2.setFirstName("Karl");
        mh2.setSurName("Marx");
        mh2.setVisits(visits);
        
        mh3.setFirstName("Karl");
        mh3.setSurName("Hess");
        mh3.setVisits(visits);
        
        mushroomHunterDao.addHunter(mh1);
        mushroomHunterDao.addHunter(mh2);
        mushroomHunterDao.addHunter(mh3);
        
        Set<MushroomHunter> hunters = new HashSet<>();
        hunters.add(mh1);
        hunters.add(mh2);
        hunters.add(mh3);
        
        List<MushroomHunter> huntersFromDao = mushroomHunterDao.getAllHunters();
        
        for(MushroomHunter mh : hunters){
            Assert.assertTrue(huntersFromDao.contains(mh));
        }    
    }
    
    @Test
    public void updateTest(){
        MushroomHunter mh1 = new MushroomHunter();

        mh1.setFirstName("Karl");
        mh1.setSurName("Franz");
        mushroomHunterDao.addHunter(mh1);
        
        mh1.setSurName("Hess");
        mushroomHunterDao.updateHunter(mh1);
                
        MushroomHunter mhDB = em.find(MushroomHunter.class, mh1.getId());
        Assert.assertNotNull(mhDB);
        Assert.assertEquals("Hess", mhDB.getSurName());
        
    }
    
    @Test
    public void relationsTest(){
        MushroomHunter mh1 = new MushroomHunter();

        mh1.setFirstName("Karl");
        mh1.setSurName("Franz");
        
        Forest forest = new Forest();
        forest.setLatitude(0);
        forest.setLongitude(0);
        forest.setName("forest1");
        forest.setDescription("none");
        
        Visit visit = new Visit();
        visit.setDateOfVisit(new Date());
        visit.setNote("none");
        visit.setForest(forest);
        visit.setHunter(mh1);
        
        Set<Visit> visits = new HashSet<>();
        visits.add(visit);
        forest.setVisits(visits);
        mh1.setVisits(visits);
        
        mushroomHunterDao.addHunter(mh1);
        Assert.assertTrue(mh1.getId()!=0L);
        
        MushroomHunter mhDB = em.find(MushroomHunter.class, mh1.getId());
        
        mhDB = em.merge(mhDB);
        Assert.assertEquals(1, mhDB.getVisits().size());
        Visit visitFromDB = new ArrayList<>(mhDB.getVisits()).get(0);
        Assert.assertEquals(visit, visitFromDB);
        
        Assert.assertEquals(forest, visitFromDB.getForest());
    }
}
