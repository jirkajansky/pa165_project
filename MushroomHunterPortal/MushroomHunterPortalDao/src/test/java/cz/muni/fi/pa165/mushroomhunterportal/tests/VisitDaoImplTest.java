package cz.muni.fi.pa165.mushroomhunterportal.tests;

import cz.muni.fi.pa165.mushroomhunterportal.dao.VisitDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Forest;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Visit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

/**
 *
 * @author Jan Trnka
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:applicationContext.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class VisitDaoImplTest {

    private Logger logger = LoggerFactory.getLogger(VisitDaoImplTest.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private VisitDao visitDao;

    @Test
    public void basicAddReadUpdateDeleteTest() {

        Visit v1 = new Visit();
        Forest f1 = new Forest();

        f1.getVisits().add(v1);
        f1.setLatitude(0);
        f1.setLongitude(0);
        f1.setName("Sherwood");
        f1.setDescription("DESC");

        v1.setForest(f1);
        v1.setNote("Visit 1");

        MushroomHunter h1 = new MushroomHunter();
        h1.setFirstName("Fn");
        h1.setSurName("Sn");
        h1.setPersonalInfo("pInfo");
        h1.getVisits().add(v1);
        v1.setHunter(h1);

        visitDao.addVisit(v1);

        Visit vFromDao = visitDao.getById(v1.getId());

        Assert.assertEquals(v1, vFromDao);

        v1.setNote("UPDATED");
        visitDao.updateVisit(v1);

        Visit vDB2 = visitDao.getById(v1.getId());
        Assert.assertEquals("UPDATED", vDB2.getNote());
        visitDao.deleteVisit(v1);

        Assert.assertNull("Visit should be deleted on deleteVisit", visitDao.getById(v1.getId()));
    }

    @Test
    public void getAllTest() {
        List<Visit> visitsFromDB;
        visitsFromDB = em.createQuery("SELECT v FROM Visit v", Visit.class).getResultList();
        Assert.assertEquals(0, visitsFromDB.size());

        Visit v1 = new Visit();
        Visit v2 = new Visit();
        Visit v3 = new Visit();

        v1.setNote("Visit 1");
        v2.setNote("Visit 2");
        v3.setNote("Visit 3");

        MushroomHunter mh1 = new MushroomHunter();
        mh1.setFirstName("Robin");
        mh1.setSurName("Hood");

        Forest f = new Forest();
        f.setName("Sherwood");
        f.setLatitude(53.200464);
        f.setLongitude(-1.113868);

        v1.setForest(f);
        f.getVisits().add(v1);
        v1.setHunter(mh1);
        mh1.getVisits().add(v1);
        visitDao.addVisit(v1);

        v2.setForest(f);
        f.getVisits().add(v2);
        v2.setHunter(mh1);
        mh1.getVisits().add(v2);
        visitDao.addVisit(v2);

        v3.setForest(f);
        f.getVisits().add(v3);
        v3.setHunter(mh1);
        mh1.getVisits().add(v3);
        visitDao.addVisit(v3);

        Assert.assertEquals(3, visitDao.getAllVisit().size());

        visitsFromDB = em.createQuery("SELECT v FROM Visit v", Visit.class).getResultList();

        List<Visit> visitsFromDao = visitDao.getAllVisit();
        for (Visit v : visitsFromDB) {
            Assert.assertTrue(visitsFromDao.contains(v));
        }
    }

    @Test
    public void getByIdTest() {

        Visit v1 = new Visit();
        Visit v2 = new Visit();
        Visit v3 = new Visit();

        v1.setNote("Visit 1");
        v2.setNote("Visit 2");
        v3.setNote("Visit 3");

        MushroomHunter mh1 = new MushroomHunter();
        mh1.setFirstName("Robin");
        mh1.setSurName("Hood");

        Forest f = new Forest();
        f.setName("Sherwood");
        f.setLatitude(53.200464);
        f.setLongitude(-1.113868);

        v1.setHunter(mh1);
        mh1.getVisits().add(v1);
        v1.setForest(f);
        f.getVisits().add(v1);
        visitDao.addVisit(v1);

        v2.setHunter(mh1);
        mh1.getVisits().add(v2);
        v2.setForest(f);
        f.getVisits().add(v2);
        visitDao.addVisit(v2);

        v3.setHunter(mh1);
        mh1.getVisits().add(v3);
        v3.setForest(f);
        f.getVisits().add(v3);
        visitDao.addVisit(v3);

        Set<Visit> visits = new HashSet<>();
        visits.add(v1);
        visits.add(v2);
        visits.add(v3);

        for (Visit v : visits) {
            Visit vDB = em.createQuery("Select v From Visit v where id= :id", Visit.class).
                    setParameter("id", v.getId()).getSingleResult();
            Visit vDaoVisit = visitDao.getById(v.getId());
            Assert.assertEquals(vDB, vDaoVisit);
        }
    }

    @Test
    public void relationTest() {

        Visit v = new Visit();

        v.setNote("Test visit");
        Forest f = new Forest();
        f.setName("Sherwood");
        f.setLatitude(0);
        f.setLongitude(0);

        MushroomHunter h = new MushroomHunter();
        h.setFirstName("Robin");
        h.setSurName("Hood");

        v.setForest(f);
        f.getVisits().add(v);
        v.setHunter(h);
        h.getVisits().add(v);

        visitDao.addVisit(v);

        Visit vDB = em.find(Visit.class, v.getId());

        Assert.assertEquals(vDB, v);

    }

    @Test
    public void cascadingTest() {

        Visit v = new Visit();
        v.setNote("Test visit");

        Forest f = new Forest();
        f.setName("Sherwood");
        f.setLatitude(0);
        f.setLongitude(0);

        MushroomHunter h = new MushroomHunter();
        h.setFirstName("Robin");
        h.setSurName("Hood");

        v.setHunter(h);
        h.getVisits().add(v);
        v.setForest(f);
        f.getVisits().add(v);

        visitDao.addVisit(v);

        MushroomHunter hDB = em.find(MushroomHunter.class, v.getHunter().getId());

        Forest fDB = em.find(Forest.class, v.getForest().getId());

        visitDao.deleteVisit(v);

        try {
            MushroomHunter hDBAfterDelete = em.find(MushroomHunter.class, v.getHunter().getId());
        } catch (NoResultException e) {
            fail("Deleting visit must not delete hunter!");
        }

        try {
            Forest fDBAfterDelete = em.find(Forest.class, v.getForest().getId());
        } catch (Exception e) {
            fail("Deleting visit must not delete forest!");
        }

    }
}
