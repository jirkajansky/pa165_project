package cz.muni.fi.pa165.mushroomhunterportal.tests;

import cz.muni.fi.pa165.mushroomhunterportal.dao.ForestDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Forest;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Visit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Test;
import javax.persistence.EntityManager;
import org.junit.Assert;
import static org.junit.Assert.fail;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

/**
 *
 * @author Martina Prochazkova
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:applicationContext.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class ForestDaoImplTest {  
    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    private ForestDao forestDao;

    @Test
    public void basicCreateDeleteTest(){
        
        Forest f1 = new Forest();
        f1.setName("Hustoles");
        f1.setLatitude(49.209222);
        f1.setLongitude(16.598895);
        f1.setDescription("none");
        forestDao.addForest(f1);
        
        Forest fDB = em.createQuery("SELECT f FROM Forest f", Forest.class).getSingleResult();
        Assert.assertEquals(f1, fDB);
        
        Forest fFromDao = forestDao.getById(fDB.getId());
        
        Assert.assertEquals(f1, fFromDao);        
        

        
        try
        {
            forestDao.deleteForest(f1);
        } catch(Exception ex)
        {
            fail("No exception should be thrown: " + ex);
        }
        
        try
        {
            fDB = em.createQuery("SELECT f FROM Forest f", Forest.class).getSingleResult();
            fail("NoResultException should be thrown");
        } catch(NoResultException ex) {}        
    }
    
    @Test
    public void getAllTest(){        
        Forest f1 = new Forest();
        Forest f2 = new Forest();
        Forest f3 = new Forest();
        
        Set<Visit> visits = new HashSet<>();
        
        f1.setName("Hustoles");
        f1.setLatitude(49.209222);
        f1.setLongitude(16.598895);
        f1.setDescription("none");
        f1.setVisits(visits);
        
        f2.setName("Luzanky");
        f2.setLatitude(49.206922);
        f2.setLongitude(16.608775);
        f2.setVisits(visits);
        f2.setDescription("none");
        
        f3.setName("Park Spilberk");
        f3.setLatitude(49.194977);
        f3.setLongitude(16.600192);
        f3.setDescription("none");
        f3.setVisits(visits);
        
        forestDao.addForest(f1);
        forestDao.addForest(f2);
        forestDao.addForest(f3);
        
        Set<Forest> forests = new HashSet<>();
        forests.add(f1);
        forests.add(f2);
        forests.add(f3);
        
        List<Forest> forestsFromDao = forestDao.getAllForests();
        
        for(Forest f : forests){
            Assert.assertTrue(forestsFromDao.contains(f));
        }    
    }
    
    @Test
    public void updateTest(){
        
        Forest f1 = new Forest();
        f1.setName("Hustoles");
        f1.setLatitude(49.209222);
        f1.setLongitude(16.598895);
        f1.setDescription("none");
        forestDao.addForest(f1);
        
        f1.setName("Hustoles New");
        forestDao.updateForest(f1);
        
        Forest fDB = em.createQuery("SELECT f FROM Forest f", Forest.class).getSingleResult();
        Assert.assertEquals("Hustoles New", fDB.getName());
        
        
    }
    
    @Test
    public void relationsTest(){
        Forest f1 = new Forest();

        f1.setName("Hustoles");
        f1.setLatitude(49.209222);
        f1.setLongitude(16.598895);
        f1.setDescription("none");
        
        MushroomHunter hunter = new MushroomHunter();
        hunter.setFirstName("Karl");
        hunter.setSurName("Franz");
        
        Visit visit = new Visit();
        visit.setDateOfVisit(new Date());
        visit.setNote("none");
        visit.setHunter(hunter);
        visit.setForest(f1);
        
        Set<Visit> visits = new HashSet<>();
        visits.add(visit);
        hunter.setVisits(visits);
        f1.setVisits(visits);
        
        forestDao.addForest(f1);
        Assert.assertFalse(f1.getId()==0);
        
        Forest fDB = em.find(Forest.class, f1.getId());
        Assert.assertEquals(1, fDB.getVisits().size());
        Visit visitFromDB = new ArrayList<>(fDB.getVisits()).get(0);
        Assert.assertEquals(visit, visitFromDB);
        
        Assert.assertEquals(hunter, visitFromDB.getHunter());        
        
    }

    @Test
    public void badInputsTest(){  
        List<Forest> forests = null;
        
        try {
            forests = forestDao.getAllForests();
        }
        catch(Exception ex) {
            fail("getAll on empty database shouldn't throw exception. Exception thrown: " + ex);
        }
        Assert.assertNotNull("getAll method should return empty list for empty database, not null", forests);
        
        Forest f1 = null;
        try {
            f1 = forestDao.getById(5L);
        }
        catch(Exception ex) {
            fail("getById method houldn't throw exception when requesting non-existing id. Thrown: " + ex);
        }
        Assert.assertNull("getById should return null when requesting non-existing id", f1);
        
        Forest f2 = new Forest();
        f2.setName("Hustoles");
        f2.setLatitude(49.209222);
        f2.setLongitude(16.598895);
        f2.setDescription("none");
        
        try {
            forestDao.deleteForest(f2);            
        }
        catch(Exception ex) {
            fail("No exception should be thrown when deleting non-persistent entity");
        }
    }
}
