package cz.muni.fi.pa165.mushroomhunterportal.tests;

import cz.muni.fi.pa165.mushroomhunterportal.dao.UserDao;
import org.springframework.data.repository.CrudRepository;
/**
 * JUnit test for {@link UserDao}. Unimplemented, no point in testing {@link CrudRepository}.
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
public class UserDaoTest {
    
}
