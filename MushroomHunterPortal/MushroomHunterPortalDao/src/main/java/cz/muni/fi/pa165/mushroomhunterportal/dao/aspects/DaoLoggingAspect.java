package cz.muni.fi.pa165.mushroomhunterportal.dao.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author Jan Bruzl
 */
@Component
@Aspect
public class DaoLoggingAspect {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Around("execution(* cz.muni.fi.pa165.mushroomhunterportal.dao.impl.*.*(..))")
    public Object traceLog(ProceedingJoinPoint  pjp) throws Throwable {        
        logger.trace("{} - {} called with arg: {}", pjp.getClass().getName(), pjp.getSignature().getName(), pjp.getArgs()); 
        Object result = pjp.proceed();
        logger.trace("{} ended", pjp.getSignature().getName()); 
        return result;
    }
    
}
