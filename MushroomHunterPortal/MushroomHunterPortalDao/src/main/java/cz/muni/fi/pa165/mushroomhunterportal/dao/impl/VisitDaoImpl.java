package cz.muni.fi.pa165.mushroomhunterportal.dao.impl;

import cz.muni.fi.pa165.mushroomhunterportal.dao.VisitDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Forest;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Visit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author Jan Bruzl
 */
@Component
public class VisitDaoImpl implements VisitDao {

    private final Logger logger = LoggerFactory.getLogger(VisitDaoImpl.class);
    @PersistenceContext
    private EntityManager em;

    @Override
    public void addVisit(Visit visit) {
        logger.info("Adding entity {}", visit);

        Forest forest = em.find(Forest.class, visit.getForest().getId());
        if (forest != null) {
            visit.setForest(forest);
        }
        MushroomHunter mh = em.find(MushroomHunter.class, visit.getHunter().getId());
        if (mh != null) {
            visit.setHunter(mh);
        }
        em.persist(visit);

        logger.info("Entity {} added", visit);
    }

    @Override
    public void deleteVisit(Visit visit) {
        logger.info("Removing {} entity", visit);

        Visit visitToDelete = em.find(Visit.class, visit.getId());

        if (visitToDelete == null) {
            logger.error("Entity to delete {} not persisted", visit);
            return;
        }
        Forest forest = visitToDelete.getForest();
        forest.getVisits().remove(visitToDelete);
        MushroomHunter hunter = visitToDelete.getHunter();
        hunter.getVisits().remove(visitToDelete);
        
        em.remove(visitToDelete);

        logger.info("Entity {} removed", visit);
    }

    @Override
    public void updateVisit(Visit visit) {
        logger.info("Updating entity {}", visit);
        em.merge(visit);
    }

    @Override
    public Visit getById(long id) {
        logger.info("Requesting entity with id: {}", id);
        return em.find(Visit.class, id);
    }

    @Override
    public List<Visit> getAllVisit() {        
        List<Visit> visits = null;
        visits = em.createQuery("SELECT v FROM Visit v", Visit.class).getResultList();           
        return visits;
    }

}
