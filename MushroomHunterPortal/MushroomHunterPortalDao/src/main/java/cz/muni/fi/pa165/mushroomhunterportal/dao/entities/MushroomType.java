package cz.muni.fi.pa165.mushroomhunterportal.dao.entities;

/**
 * Mushroom type enum
 * 
 * @author Jan Bruzl
 */
public enum MushroomType {
    EDIBLE, UNEDIBLE, POISONOUS
}
