package cz.muni.fi.pa165.mushroomhunterportal.dao;

import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Forest;
import java.util.List;

/**
 * DAO interface for Forest entities
 * 
 * @author Jan Bruzl
 */
public interface ForestDao {
    /**
     * Persists new Forest entity.
     * 
     * @param forest 
     */
    public void addForest(Forest forest);
    
    /**
     * Update persisted Forest entity.
     * 
     * @param forest 
     */
    public void updateForest(Forest forest);
    
    /**
     * Delete persisted Forest enity.
     * 
     * @param forest 
     */
    public void deleteForest(Forest forest);
    
    /**
     * Returns all persisted Forest entities.
     * 
     * @return forests
     */
    public List<Forest> getAllForests();
    
    /**
     * Returns Forest entity with given id.
     * 
     * @param id
     * @return forest
     */
    public Forest getById(long id);
    
}
