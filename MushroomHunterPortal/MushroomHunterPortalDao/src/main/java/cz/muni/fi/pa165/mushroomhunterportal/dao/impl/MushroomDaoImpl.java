package cz.muni.fi.pa165.mushroomhunterportal.dao.impl;

import cz.muni.fi.pa165.mushroomhunterportal.dao.MushroomDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Mushroom;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author Martina Prochazkova
 */
@Component
public class MushroomDaoImpl implements MushroomDao{
    
    private final Logger logger = LoggerFactory.getLogger(VisitDaoImpl.class);
    @PersistenceContext
    private EntityManager em;
    
    /**
     * add mushroom
     * @param mushroom 
     */
    @Override
    public void addMushroom(Mushroom mushroom) {    
        logger.info("Adding entity {}", mushroom);       
        em.persist(mushroom);
        logger.info("Entity {} added", mushroom);
    }
    
    /**
     * update mushroom
     * @param mushroom 
     */
    @Override
    public void updateMushroom(Mushroom mushroom) {
        logger.info("Updating entity {}", mushroom);
        em.merge(mushroom);
    }

    /**
     * delete mushroom
     * @param mushroom 
     */
    @Override
    public void deleteMushroom(Mushroom mushroom) {
        logger.info("Removing {} entity", mushroom);

        Mushroom mushroomToDelete = em.find(Mushroom.class, mushroom.getId());

        if (mushroomToDelete == null) {
            logger.error("Entity to delete {} not persisted", mushroom);
            return;
        }
        
        em.remove(mushroomToDelete);

        logger.info("Entity {} removed", mushroom);
    }
    
    /**
     * 
     * @return all mushroom
     */
    @Override
    public List<Mushroom> getAllMushrooms() {
        List<Mushroom> mushrooms = null;
        mushrooms = em.createQuery("Select m from Mushroom m", Mushroom.class).getResultList();           
        return mushrooms;
    }

    /**
     * 
     * @param id
     * @return mushroom by id
     */
    @Override
    public Mushroom getById(long id) {
        logger.info("Requesting entity with id: {}", id);
        return em.find(Mushroom.class, id);

    }  
}
