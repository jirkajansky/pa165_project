package cz.muni.fi.pa165.mushroomhunterportal.dao;

import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Visit;
import java.util.List;

/**
 * Dao Interface
 * @author Jiri Jansky
 */
public interface VisitDao {
    /**
     * add visit
     * @param visit 
     */
    public void addVisit(Visit visit);
    /**
     * delete visit
     * @param visit 
     */
    public void deleteVisit(Visit visit);
    /**
     * update visit
     * @param visit 
     */
    public void updateVisit(Visit visit);
    /**
     * get visit by id
     * @param id
     * @return 
     */
    public Visit getById(long id);
    /**
     * get collection of all visit
     * @return collection of Visits 
     */
    List<Visit> getAllVisit();
}
