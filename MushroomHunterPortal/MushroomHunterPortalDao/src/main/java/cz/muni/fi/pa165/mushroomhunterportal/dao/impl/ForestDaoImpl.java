package cz.muni.fi.pa165.mushroomhunterportal.dao.impl;

import cz.muni.fi.pa165.mushroomhunterportal.dao.ForestDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Forest;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author Jan Trnka
 */
@Component
public class ForestDaoImpl implements ForestDao {

    private Logger logger = LoggerFactory.getLogger(ForestDaoImpl.class);

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void addForest(Forest forest) {
        logger.info("Adding entity {}", forest);
        em.persist(forest);
        logger.info("Entity {} added", forest);
    }

    @Override
    public void updateForest(Forest forest) {
        logger.info("Updating entity {}", forest);
        em.merge(forest);
    }

    @Override
    public void deleteForest(Forest forest) {
        logger.info("Removing {} entity", forest);

        Forest forestToDelete = em.find(Forest.class, forest.getId());
        if (forestToDelete == null) {
            logger.error("Entity to delete {} not persisted", forest);
            return;
        }
        em.remove(forestToDelete);
    }

    @Override
    public List<Forest> getAllForests() {
        List<Forest> forests = null;
        forests = em.createQuery("Select f from Forest f", Forest.class).
                getResultList();
        return forests;
    }

    @Override
    public Forest getById(long id) {
        logger.info("Requesting entity with id: {}", id);
        return em.find(Forest.class, id);
    }

}
