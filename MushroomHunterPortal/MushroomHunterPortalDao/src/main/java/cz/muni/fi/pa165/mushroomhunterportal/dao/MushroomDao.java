package cz.muni.fi.pa165.mushroomhunterportal.dao;

import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.Mushroom;

import java.util.List;

/**
 * DAO interface for Mushroom entities.
 * 
 * @author Jan Trnka
 */
public interface MushroomDao {

    /**
     * Persists new Mushroom entity.
     * @param mushroom 
     */
    void addMushroom(Mushroom mushroom);

    /**
     * Updates persisted Mushroom entity.
     * 
     * @param mushroom 
     */
    void updateMushroom(Mushroom mushroom);

    /**
     * Deletes persisted Mushroom entity.
     * 
     * @param mushroom 
     */
    void deleteMushroom(Mushroom mushroom);

    /**
     * Returns all persisted Mushroom entities.
     * 
     * @return mushroom
     */
    public List<Mushroom> getAllMushrooms();
    
    /**
     * Returns Mushroom entity with given id.
     * @param id
     * @return mushroom
     */
    public Mushroom getById(long id);
}
