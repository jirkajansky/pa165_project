
package cz.muni.fi.pa165.mushroomhunterportal.dao.entities;

/**
 * Defined roles for {@link User}
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
public enum Role {
    ROLE_USER, ROLE_ADMINISTRATOR;
}
