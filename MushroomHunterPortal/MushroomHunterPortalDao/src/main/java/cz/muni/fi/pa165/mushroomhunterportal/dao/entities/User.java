package cz.muni.fi.pa165.mushroomhunterportal.dao.entities;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * User entity
 * 
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
@Entity
@Table(name="MHUser")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    private String mail;
    
    @OneToOne
    private MushroomHunter hunter;
    
    private Role role;
    
    private String password;

    
    
    public User() {
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", mail=" + mail + ", hunter=" + hunter + ", role=" + role + '}';
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = new String(password.toCharArray());
    }

    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public MushroomHunter getHunter() {
        return hunter;
    }

    public void setHunter(MushroomHunter hunter) {
        this.hunter = hunter;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
    
    
}
