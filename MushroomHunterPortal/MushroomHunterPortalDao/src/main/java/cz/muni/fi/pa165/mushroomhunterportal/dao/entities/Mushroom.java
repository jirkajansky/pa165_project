package cz.muni.fi.pa165.mushroomhunterportal.dao.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


/**
 * Mushroom entity
 * @author Jan Bruzl
 */
@Entity
public class Mushroom {
    @Id
    @GeneratedValue
    private long id;
    
    @Column(length = 255, nullable = false)
    private String name;
    
    @Enumerated(EnumType.STRING)
    private MushroomType type;
    
    private String occurence;

    public Mushroom() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MushroomType getType() {
        return type;
    }

    public void setType(MushroomType type) {
        this.type = type;
    }

    public String getOccurence() {
        return occurence;
    }

    public void setOccurence(String occurence) {
        this.occurence = occurence;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mushroom other = (Mushroom) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Mushroom{" + "id=" + id + ", name=" + name + ", type=" + type + ", occurence=" + occurence + '}';
    }

    
}
