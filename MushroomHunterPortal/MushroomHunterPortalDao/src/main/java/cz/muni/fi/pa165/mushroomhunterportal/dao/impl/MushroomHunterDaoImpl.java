/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.mushroomhunterportal.dao.impl;


import cz.muni.fi.pa165.mushroomhunterportal.dao.MushroomHunterDao;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;

/**
 * Implementation of Dao method fot mushroom hunter
 * @author Jiri Jansky
 */
@Component
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class MushroomHunterDaoImpl implements MushroomHunterDao
{
    private final Logger logger = LoggerFactory.getLogger(VisitDaoImpl.class);

    @PersistenceContext
    private EntityManager em;

    /**
     * add hunter
     * @param hunter 
     */
    @Override
    public void addHunter(MushroomHunter hunter) {
        logger.info("Adding entity {}", hunter);
        em.persist(hunter);        
        logger.info("Entity {} added", hunter);
    }

    /**
     * udpate hunter
     * @param hunter 
     */
    @Override
    public void updateHunter(MushroomHunter hunter) { 
        logger.info("Updating entity {}", hunter);
        em.merge(hunter);
    }

    /**
     * delete hunter
     * @param hunter 
     */
    @Override
    public void deleteHunter(MushroomHunter hunter) {
        logger.info("Removing {} entity", hunter);

        MushroomHunter hunterToDelete = em.find(MushroomHunter.class, hunter.getId());

        if (hunterToDelete == null) {
            logger.error("Entity to delete {} not persisted", hunter);
            return;
        }
        em.remove(hunterToDelete);

        logger.info("Entity {} removed", hunterToDelete);
    }
    
    /**
     * 
     * @param id
     * @return hunter by id
     */
    @Override
    public MushroomHunter getById(long id) {
        logger.info("Requesting entity with id: {}", id);
        MushroomHunter hunter = null;
        try
        {
            hunter = em.createQuery("SELECT m FROM MushroomHunter m WHERE Id = :id", MushroomHunter.class).
                            setParameter("id", id).
                            getSingleResult();
        }
        catch(Exception ex)
        {}
       
        return hunter;
    }

    /**
     * 
     * @return all hunters
     */
    @Override
    public List<MushroomHunter> getAllHunters() {
        List<MushroomHunter> hunters = null;

        hunters = em.createQuery("SELECT m FROM MushroomHunter m", MushroomHunter.class).getResultList();        
        return hunters;
    }


}
