package cz.muni.fi.pa165.mushroomhunterportal.dao.entities;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * Visit entity
 * @author Martina Prochazkova
 */
@Entity
public class Visit {
    @Id
    @GeneratedValue
    private long id;
    
    @ManyToOne(cascade = CascadeType.PERSIST,  fetch = FetchType.LAZY) 
    private MushroomHunter hunter;
    
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Forest forest;
    
    @Temporal(TemporalType.DATE)
    private Date dateOfVisit;
    
    @Column(nullable = false)
    private String note;

    public Visit() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public MushroomHunter getHunter() {
        return hunter;
    }

    public void setHunter(MushroomHunter hunter) {
        this.hunter = hunter;
    }
    
    public Forest getForest() {
        return forest;
    }

    public void setForest(Forest forest) {
        this.forest = forest;
    }
    
    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }
    
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Visit other = (Visit) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Visit{" + "id=" + id + ", hunter=" + hunter + ", forest=" + forest + ", dateOfVisit=" + dateOfVisit + ", note=" + note + '}';
    }
    
}
