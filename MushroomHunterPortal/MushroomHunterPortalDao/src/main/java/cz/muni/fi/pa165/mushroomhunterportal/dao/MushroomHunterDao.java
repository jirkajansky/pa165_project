package cz.muni.fi.pa165.mushroomhunterportal.dao;

import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import java.util.List;

/**
 *  * DAO interface for MushroomHunter entities
 *
 * @author Martina Prochazkova
 */
public interface MushroomHunterDao {
    
    /**
     * Persists new MushroomHunter entity.
     * 
     * @param hunter 
     */
    public void addHunter(MushroomHunter hunter);
    
    /**
     * Update persisted MushroomHunter entity.
     * 
     * @param hunter 
     */
    public void updateHunter(MushroomHunter hunter);
    
    /**
     * Delete persisted MushroomHunter entity.
     * 
     * @param hunter 
     */
    public void deleteHunter(MushroomHunter hunter);
    
    /**
     * TODO
     * @param id
     * @return 
     */
    public MushroomHunter getById(long id);
   
    /**
     * Returns all persisted MushroomHunter entities.
     * 
     * @return hunters
     */
    public List<MushroomHunter> getAllHunters();
    
}
