
package cz.muni.fi.pa165.mushroomhunterportal.dao;

import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.MushroomHunter;
import cz.muni.fi.pa165.mushroomhunterportal.dao.entities.User;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
public interface UserDao extends CrudRepository<User, Long>{
    /**
     * Returns user entity with given mail
     * @param mail
     * @return {@link User}
     */
    public User getByMail(String mail);
    
    /**
     * Returns user entity with given hunter
     * @param mushroomHunter {@link MushroomHunter}
     * @return {@link User}
     */
    public User getByHunter(MushroomHunter mushroomHunter);
}
