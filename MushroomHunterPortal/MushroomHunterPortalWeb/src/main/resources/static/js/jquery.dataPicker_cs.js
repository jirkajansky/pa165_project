/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$.datepicker.regional['cs'] = {
                closeText: 'Hotovo',
                currentText: 'Dnes',
                prevText: 'Předchozí',
                nextText: 'Další',
                monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
                monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'],
                dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'],
                dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So'],
                dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'],
                weekHeader: 'č.T',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''}; 
$.datepicker.setDefaults($.datepicker.regional['cs']); 