package cz.muni.fi.pa165.mushroomhunterportal.web.validators;


import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
/**
 * validator for hunter controller
 * @author Jiri Jansky
 */
public class MushroomHunterValidator implements Validator {
        
    /**
     * return true for supports class
     * @param paramClass
     * @return 
     */
    @Override
    public boolean supports(Class<?> paramClass) {
        return MushroomHunterDTO.class.equals(paramClass);
    } 
    
    /**
     * validation for hunter
     * @param obj
     * @param errors 
     */
    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "id.required");
        if (obj == null)
            return;
        MushroomHunterDTO hunter = (MushroomHunterDTO) obj;
        if(hunter.getId() < 0){
            errors.rejectValue("id", "negativeValue", new Object[]{"'id'"}, "id can't be negative");
        }
         
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "firstName.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surName", "surName.required");
    }
    
}
