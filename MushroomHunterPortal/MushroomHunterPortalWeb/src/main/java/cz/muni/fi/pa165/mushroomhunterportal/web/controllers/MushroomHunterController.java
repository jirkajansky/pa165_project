package cz.muni.fi.pa165.mushroomhunterportal.web.controllers;

import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomHunterService;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.VisitService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.VisitDTO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Jan Trnka <trnkajn@gmail.com>
 */
@Controller
public class MushroomHunterController {

    private final Logger logger = LoggerFactory.getLogger(MushroomController.class);

    @Autowired
    private MushroomHunterService mushroomHunterService;

    @Autowired
    private VisitService visitService;
    
    @Autowired
    @Qualifier("mushroomHunterValidator")
    private Validator validator;
 
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping("/hunters")
    public void mushroomHunters() {
    }

    @ModelAttribute("mushroomHunterDTO")
    public MushroomHunterDTO getNewHunter() {
        return new MushroomHunterDTO();
    }

    @ModelAttribute("allHunters")
    public List<MushroomHunterDTO> getAllMushroomHunters() {
        return mushroomHunterService.getAllHunters();
    }

    @RequestMapping("/hunters/detail")
    public void mushroomHunterDetail(
            @RequestParam(required = true, value = "id") long id, Model model) {
        model.addAttribute("id", id);
        MushroomHunterDTO hunter;
        try {
            hunter = mushroomHunterService.getById(id);
        } catch (DataAccessException e) {
            logger.error("Failed to obtain hunter with id: {}, {}", id, e);
            hunter = null;
        }
        model.addAttribute("mushroomHunterDTO", hunter);
        List<VisitDTO> visits = visitService.getVisitsByHunter(hunter);
        model.addAttribute("visits", visits);

    }


    @RequestMapping("hunters/edit")
    public void mushroomHunterEdit(@RequestParam(required = true, value = "id") long id, Model model) {
        model.addAttribute("id", id);

        MushroomHunterDTO hunter;
        try {
            hunter = mushroomHunterService.getById(id);
        } catch (DataAccessException e) {
            logger.error("Failed to obtain hunter with id: {}, {}", id, e);
            hunter = null;
        }
        model.addAttribute("mushroomHunterDTO", hunter);
    }


    @RequestMapping(value = "/hunters/edit-submit", params = {"save"})
    public String mushroomHunterEditSubmit(@Validated final MushroomHunterDTO hunter, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            logger.info("Returning error page");
            model.addAttribute("mushroomHunterDTO", hunter);
            return "/hunters/edit";
        }
        try {
            mushroomHunterService.updateHunter(hunter);
        } catch (DataAccessException e) {
            logger.error("Failed to update hunter: {}, {}", hunter, e);
        }
        return "redirect:/hunters";
    }

    @RequestMapping("/hunters/delete")
    public String mushroomHunterDelete(@RequestParam(required = true, value = "id") long id) {
        MushroomHunterDTO hunter;
        try {
            hunter = mushroomHunterService.getById(id);
        } catch (DataAccessException e) {
            logger.error("Failed to obtain hunter with id: {}, {}", id, e);
            return "redirect:/hunters";
        }

        try {
            mushroomHunterService.deleteHunter(hunter);
        } catch (DataAccessException e) {
            logger.error("Delete: Failed to delete mushroom with id: {}, {}", id, e);
        }
        return "redirect:/hunters";
    }
}
