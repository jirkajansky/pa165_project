package cz.muni.fi.pa165.mushroomhunterportal.web.controllers;

import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.ForestService;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomHunterService;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.VisitService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.ForestDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.UserDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.VisitDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.UserService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * MVC controller for visit
 *
 * @author Jiri Jansky
 */
@Controller
public class VisitController {

    private final Logger logger = LoggerFactory.getLogger(VisitController.class);

    @Autowired
    UserService userService;
        
    @Autowired
    private MushroomHunterService mushroomHunterService;

    @Autowired
    private ForestService forestService;
    
    @Autowired
    private VisitService visitService;
    
    @Autowired
    @Qualifier("visitValidator")
    private Validator validator;
    
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    /**
     * on visits
     */
    @RequestMapping("/visits")
    public void visits() {
        
    }
    
    /**
     * 
     * @return new visit for model
     */
    @ModelAttribute("visitDTO")
    public VisitDTO getVisit() {
        VisitDTO visit = new VisitDTO();
        visit.setHunterId(userService.getLogedUser().getHunter().getId());
        return visit;
    }

    /**
     * 
     * @return all visits
     */
    @ModelAttribute("allVisits")
    public List<VisitDTO> getAllVisits() {
        return visitService.getAllVisits();
    }
    
    /**
     * 
     * @return all forests
     */
    @ModelAttribute("allForests")
    public List<ForestDTO> getAllForests() {
        return forestService.getAllForests();
    }

    /**
     * 
     * @return all hunters
     */
    @ModelAttribute("allHunters")
    public List<MushroomHunterDTO> getAllMushroomHunters() {
        return mushroomHunterService.getAllHunters();
    }

    /**
     * set model for view detail if visit by id
     *
     * @param id
     * @param model 
     */
    @RequestMapping("/visits/detail")
    public void visitDetail(@RequestParam(required = true, value = "id") long id, Model model) {
        model.addAttribute("id", id);

        VisitDTO visit;
        try {
            visit = visitService.getVisitById(id);
        } catch (DataAccessException ex) {
            logger.error("Failed to obtain visit with id: {}, {}", id, ex);
            visit = null;
        }

        model.addAttribute("visitDTO", visit);
    }

    /**
     * for add view 
     *
     * @param model 
     */
    @RequestMapping("/visits/add")
    public void visitsAdd(Model model) {
        model.addAttribute("visitUserHunterName", userService.getLogedUser().getHunter().getAllName());
        
    }

    /**
     * for edit view
     *
     * @param id
     * @param model 
     */
    @RequestMapping("/visits/edit")
    public void visitEdit(@RequestParam(required = true, value = "id") long id, Model model) {
        model.addAttribute("id", id);
        
        VisitDTO visit;
        try {
            visit = visitService.getVisitById(id);

        } catch (DataAccessException ex) {
            logger.error("Failed to obtain Visit with id: {}, {}", id, ex);
            visit = null;
        }

        model.addAttribute("visitDTO", visit);
    }
    
    /**
     * adding visit from request sumit
     *
     * @param visit
     * @param bindingResult
     * @param model
     * @return route path 
     */
    @RequestMapping(value = "/visits/add-submit", params = {"save"}, method = RequestMethod.POST)
    public String visitAddSubmit(
            @Validated final VisitDTO visit,BindingResult bindingResult, Model model)
    {    
        
        if (bindingResult.hasErrors()) {
            logger.info("Returning error page");
            model.addAttribute("visitDTO", visit);
            return "/visits/add";
        }

        SimpleDateFormat dateParser = new SimpleDateFormat("dd.MM.yyyy");
        try {  
            
            Date dat = dateParser.parse(visit.getDateOfVisitStr());
            visit.setDateOfVisit(dat);
            
            ForestDTO forest = forestService.getForestById(visit.getForestId());
            visit.setForest(forest);
            MushroomHunterDTO hunter = mushroomHunterService.getById(visit.getHunterId());
            visit.setHunter(hunter);

            visitService.createVisit(visit);
        } catch (DataAccessException ex) {
            logger.error("Failed to create Visit : {}, {}", visit, ex);
            model.addAttribute("visitDTO", visit);
            return "/visits/add";

        } catch (ParseException ex) {
            logger.error("Failed to create Visit : {}, {}", visit, ex);
            model.addAttribute("visitDTO", visit);
            return "/visits/add";
        }
        return "redirect:/visits";
    }

    /**
     * edit visit from request submit
     *
     * @param visit
     * @param bindingResult
     * @param model
     * @return route path
     */
    @RequestMapping(value = "/visits/edit-submit", params = {"save"}, method = RequestMethod.POST)
    public String visitEditSubmit(
            @Validated final VisitDTO visit,BindingResult bindingResult, Model model)
    {
        
        
        if (bindingResult.hasErrors()) {
            logger.info("Returning error page");
            model.addAttribute("visitDTO", visit);
            return "/visits/edit";
        }

        SimpleDateFormat dateParser = new SimpleDateFormat("dd.MM.yyyy");
        try {  
            
            Date dat = dateParser.parse(visit.getDateOfVisitStr());
            visit.setDateOfVisit(dat);
            
            ForestDTO forest = forestService.getForestById(visit.getForestId());
            visit.setForest(forest);
            MushroomHunterDTO hunter = mushroomHunterService.getById(visit.getHunterId());
            visit.setHunter(hunter);

            visitService.updateVisit(visit);
        } catch (DataAccessException ex) {
            logger.error("Failed to update visit : {}, {}", visit, ex);            
            model.addAttribute("visitDTO", visit);
            return "/visits/edit";
        } catch (ParseException ex) {
            logger.error("Failed to create Visit : {}, {}", visit, ex);
            model.addAttribute("visitDTO", visit);
            return "/visits/edit";
        }
        return "redirect:/visits";
    }
    
    /**
     * delete visit
     *
     * @param id
     * @return route path
     */
    @RequestMapping("/visits/delete")
    public String visitDelete(@RequestParam(required = true, value = "id") long id) 
    {
        VisitDTO visit=visitService.getVisitById(id);
        UserDTO lUser = userService.getLogedUser();
        if (!( lUser != null && lUser.getHunter() != null && visit.getHunter()==lUser.getHunter()))
        {
            return "redirect:/visits";
        }
        try {
            visit = visitService.getVisitById(id);
        } catch (DataAccessException ex) {
            logger.error("Delete: Failed to obtain visit with id: {}, {}", id, ex);
            return "redirect:/visits";
        }

        try {
            visitService.deleteVisit(visit);
        } catch (DataAccessException ex) {
            logger.error("Delete: Failed to delete visit with id: {}, {}", id, ex);
        }

        return "redirect:/visits";
    }

}
