package cz.muni.fi.pa165.mushroomhunterportal.web.validators;

import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.VisitDTO;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
/**
 * validator for visit controller
 * @author Jiri Jansky
 */
public class VisitValidator implements Validator {
    
    /**
     * return true for supports class
     * @param paramClass
     * @return 
     */
    @Override
    public boolean supports(Class<?> paramClass) {
        return VisitDTO.class.equals(paramClass);
    } 
    
    /**
     * validation for visit
     * @param obj
     * @param errors 
     */
    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "id.required");
         
        VisitDTO visit = (VisitDTO) obj;
        if(visit.getId() < 0){
            errors.rejectValue("id", "negativeValue", new Object[]{"'id'"}, "id can't be negative");
        }
         
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dateOfVisitStr", "dateOfVisit.required");   

        Pattern r = Pattern.compile("\\d{2}\\.\\d{2}\\.\\d{4}");

        Matcher m = r.matcher(visit.getDateOfVisitStr());
        if (!m.find())
            errors.rejectValue("dateOfVisitStr", "dateOfVisitStr.badformat");
        
    }
}
