package cz.muni.fi.pa165.mushroomhunterportal.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
@Controller
public class IndexController {
    
    @RequestMapping(value = {"/index"})
    public void index(){
    }
    
    @RequestMapping(value = {"/"})
    public String homepage(){
        return "index";
    }
    
    @ModelAttribute("Welcome")
    public String welcome(){
        return "Welcome to our portal";
    }
}
