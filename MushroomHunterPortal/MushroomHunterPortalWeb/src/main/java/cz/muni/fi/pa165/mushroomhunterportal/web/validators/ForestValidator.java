package cz.muni.fi.pa165.mushroomhunterportal.web.validators;

import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.ForestDTO;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


/**
 * Validator for forest controller
 * @author Jiri Jansky
 */
public class ForestValidator implements Validator 
{
    /**
     * return true for supports class
     * @param paramClass
     * @return 
     */
    @Override
    public boolean supports(Class<?> paramClass) {
        return ForestDTO.class.equals(paramClass);
    } 
    
    /**
     * validation for forest
     * @param obj
     * @param errors 
     */
    @Override
    public void validate(Object obj, Errors errors) 
    {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "id.required");
         
        ForestDTO forest = (ForestDTO) obj;
        if(forest.getId() < 0){
            errors.rejectValue("id", "negativeValue", new Object[]{"'id'"}, "id can't be negative");
        }
         
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "latitudeStr", "latitude.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "longitudeStr", "longitude.required");
        if (!forest.getLatitudeStr().isEmpty() && !forest.getLongitudeStr().isEmpty())
        {
            try
            {
                forest.setLatitude(Double.parseDouble(forest.getLatitudeStr()));                
            }
            catch(NumberFormatException ex)
            {
                errors.rejectValue("latitudeStr", "latitude.badformat");
            }
            
            try
            {
                forest.setLongitude(Double.parseDouble(forest.getLongitudeStr()));
            }
            catch(NumberFormatException ex)
            {
                errors.rejectValue("longitudeStr", "longitude.badformat");
            }                        
        }
    }
    
}
