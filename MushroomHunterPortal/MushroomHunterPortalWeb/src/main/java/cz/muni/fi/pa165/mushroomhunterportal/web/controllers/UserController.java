package cz.muni.fi.pa165.mushroomhunterportal.web.controllers;

import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.RoleDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.UserDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomHunterService;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.UserService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Jiri Jansky
 */
@Controller
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private MushroomHunterService mushroomHunterService;

    @Autowired
    private PasswordEncoder encoder;
    
    @Autowired
    @Qualifier("userValidator")
    private Validator validator;
 
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
    
    @RequestMapping("/users")
    public void users() {
    }

    @ModelAttribute("userDTO")
    public UserDTO getUser() {
        return new UserDTO();
    }
    
    
    
    /**
     * Returns orphans mushroom hunter (without logging crendentials)
     * 
     * @return all hunters
     */
    @ModelAttribute("orphanHunters")
    public List<MushroomHunterDTO> getOrphanMushroomHunters() {
        List<MushroomHunterDTO> hunters = mushroomHunterService.getAllHunters();
        List<UserDTO> users = userService.getAll();
        
        for(UserDTO userDTO : users){
            if(hunters.contains(userDTO.getHunter()))
                hunters.remove(userDTO.getHunter());
            else{
                logger.warn("Found user without hunter! {}", userDTO);
            }
        }
            
        return hunters;
    }

    @ModelAttribute("allUsers")
    public List<UserDTO> getAllUsers() {
        return userService.getAll();
    }


    @RequestMapping("/users/detail")
    public void userDetail(@RequestParam(required = true, value = "id") long id, Model model) {
        model.addAttribute("id", id);
        UserDTO user;
        try {
            user = userService.getById(id);
        } catch (DataAccessException ex) {
            logger.error("Failed to obtain user with id: {}, {}", id, ex);
            user = null;
        }

        model.addAttribute("userDTO", user);
    }

    @RequestMapping("/users/add")
    public void userAdd(Model model) {

    }

    @RequestMapping("/users/edit")
    public void userEdit(@RequestParam(required = true, value = "id") long id, Model model) {
        model.addAttribute("id", id);

        UserDTO user;
        try {
            user = userService.getById(id);
        } catch (DataAccessException ex) {
            logger.error("Failed to obtain user with id: {}, {}", id, ex);
            user = null;
        }

        model.addAttribute("userDTO", user);
    }

    @RequestMapping(value = "/users/add-submit", params = {"save"}, method = RequestMethod.POST)
    public String userAddSubmit(
            @Validated final UserDTO user,            
            BindingResult bindingResult,
            @RequestParam(value = "password", required = true) String password,
            Model model) 
    {
        if (bindingResult.hasErrors()) {
            logger.info("Returning error page");
            model.addAttribute("userDTO", user);
            return "/users/add";
        }
        try {
            MushroomHunterDTO hunter = mushroomHunterService.getById(user.getHunterId());
            user.setHunter(hunter);
            user.setRole(RoleDTO.ROLE_USER);
            userService.createUser(user, encoder.encode(password));
        } catch (DataAccessException ex) {
            logger.error("Failed to create user : {}, {}", user, ex);
        }
        return "redirect:/users";
    }

    @RequestMapping(value = "/users/edit-submit", params = {"save"}, method = RequestMethod.POST)
    public String userEditSubmit(
            @Validated final UserDTO user,
            BindingResult bindingResult, 
            Model model) {
        if (bindingResult.hasErrors()) {
            logger.info("Returning error page");
            model.addAttribute("userDTO", user);
            return "/users/edit";
        }
        try {
            MushroomHunterDTO hunter = mushroomHunterService.getById(user.getHunterId());
            user.setHunter(hunter);
            userService.updateUser(user);
        } catch (DataAccessException ex) {
            logger.error("Failed to update user : {}, {}", user, ex);
        }
        return "redirect:/users";
    }

    @RequestMapping("/users/delete")
    public String userDelete(@RequestParam(required = true, value = "id") long id) {
        UserDTO user;
        try {
            user = userService.getById(id);
        } catch (DataAccessException ex) {
            logger.error("Delete: Failed to obtain user with id: {}, {}", id, ex);
            return "redirect:/users";
        }

        try {
            userService.deleteUser(user);
        } catch (DataAccessException ex) {
            logger.error("Delete: Failed to delete user with id: {}, {}", id, ex);
        }

        return "redirect:/users";
    }

    
}
