package cz.muni.fi.pa165.mushroomhunterportal.web.controllers;

import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomTypeDTO;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
@Controller
public class MushroomController  {

    private final Logger logger = LoggerFactory.getLogger(MushroomController.class);

    @Autowired
    private MushroomService mushroomService;

    @Autowired
    @Qualifier("mushroomValidator")
    private Validator validator;
 
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping("/mushrooms")
    public void mushrooms() {
    }

    @ModelAttribute("mushroomDTO")
    public MushroomDTO getNewMushroom() {
        return new MushroomDTO();
    }

    @ModelAttribute("allMushrooms")
    public List<MushroomDTO> getAllMushrooms() {
        return mushroomService.getAllMushrooms();
    }

    @ModelAttribute("edibleMushrooms")
    public List<MushroomDTO> getEdibleMushrooms() {
        return mushroomService.getEdibleMushrooms();
    }

    @ModelAttribute("unedibleMushrooms")
    public List<MushroomDTO> getUnedibleMushrooms() {
        return mushroomService.getUnedibleMushrooms();
    }

    @ModelAttribute("poisonousMushrooms")
    public List<MushroomDTO> getPoisonousMushrooms() {
        return mushroomService.getPoisonousMushrooms();
    }

    @ModelAttribute("mushroomType")
    public List<MushroomTypeDTO> getMushroomType() {
        List<MushroomTypeDTO> mushroomTypes = new ArrayList<MushroomTypeDTO>();

        mushroomTypes.add(MushroomTypeDTO.EDIBLE);
        mushroomTypes.add(MushroomTypeDTO.UNEDIBLE);
        mushroomTypes.add(MushroomTypeDTO.POISONOUS);

        return mushroomTypes;
    }


    @RequestMapping("/mushrooms/detail")
    public void mushroomDetail(@RequestParam(required = true, value = "id") long id, Model model) {
        model.addAttribute("id", id);

        MushroomDTO mushroom;
        try {
            mushroom = mushroomService.getMushroomById(id);
        } catch (DataAccessException ex) {
            logger.error("Failed to obtain mushroom with id: {}, {}", id, ex);
            mushroom = null;
        }

        model.addAttribute("mushroomDTO", mushroom);
    }

    @RequestMapping("/mushrooms/add")
    public void mushroomAdd(Model model) {

    }

    @RequestMapping("/mushrooms/edit")
    public void mushroomEdit(@RequestParam(required = true, value = "id") long id, Model model) {
        model.addAttribute("id", id);

        MushroomDTO mushroom;
        try {
            mushroom = mushroomService.getMushroomById(id);
        } catch (DataAccessException ex) {
            logger.error("Failed to obtain mushroom with id: {}, {}", id, ex);
            mushroom = null;
        }

        model.addAttribute("mushroomDTO", mushroom);
    }

    @RequestMapping(value = "/mushrooms/add-submit", params = {"save"}, method = RequestMethod.POST)
    public String mushroomAddSubmit(@Validated final MushroomDTO mushroom,BindingResult bindingResult, Model model) 
    {
        if (bindingResult.hasErrors()) {
            logger.info("Returning error page");
            model.addAttribute("mushroomDTO", mushroom);
            return "/mushrooms/add";
        }
        try {
            mushroomService.createMushroom(mushroom);
        } catch (DataAccessException ex) {
            logger.error("Failed to create mushroom : {}, {}", mushroom, ex);
        }
        return "redirect:/mushrooms";
    }

    @RequestMapping(value = "/mushrooms/edit-submit", params = {"save"}, method = RequestMethod.POST)
    public String mushroomEditSubmit(@Validated final MushroomDTO mushroom,BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            logger.info("Returning error page");
            model.addAttribute("mushroomDTO", mushroom);
            return "/mushrooms/edit";
        }
        try {
            mushroomService.updateMushroom(mushroom);
        } catch (DataAccessException ex) {
            logger.error("Failed to update mushroom : {}, {}", mushroom, ex);
        }
        return "redirect:/mushrooms";
    }

    @RequestMapping("/mushrooms/delete")
    public String mushroomDelete(@RequestParam(required = true, value = "id") long id) {
        MushroomDTO mushroom;
        try {
            mushroom = mushroomService.getMushroomById(id);
        } catch (DataAccessException ex) {
            logger.error("Delete: Failed to obtain mushroom with id: {}, {}", id, ex);
            return "redirect:/mushrooms";
        }

        try {
            mushroomService.deleteMushroom(mushroom);
        } catch (DataAccessException ex) {
            logger.error("Delete: Failed to delete mushroom with id: {}, {}", id, ex);
        }

        return "redirect:/mushrooms";
    }

}
