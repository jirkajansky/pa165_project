package cz.muni.fi.pa165.mushroomhunterportal.web.rest.controllers;

import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomTypeDTO;
import static cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomTypeDTO.EDIBLE;
import static cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomTypeDTO.POISONOUS;
import static cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomTypeDTO.UNEDIBLE;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.RoleDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * rest controller for mushroom
 *
 * @author Jiri Jansky
 */
@RequestMapping("/rest/mushrooms/")
@RestController
public class MushroomRestController {

    private final Logger logger = LoggerFactory.getLogger(MushroomRestController.class);

    @Autowired
    private MushroomService mushroomService;

    @Autowired
    @Qualifier("mushroomValidator")
    private Validator validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping("/test")
    public String test() {
        return "Hello world!";
    }

    /**
     * Set security context for REST controllers for Service layer
     * authentication & authorization.
     */
    private void login() {
        SecurityContext ctx = SecurityContextHolder.createEmptyContext();
        SecurityContextHolder.setContext(ctx);
        List<GrantedAuthority> gaList = new ArrayList<>();
        gaList.add(new SimpleGrantedAuthority(RoleDTO.ROLE_ADMINISTRATOR.toString()));
        User user = new User("rest", "rest",
                true, true, true, true, gaList);
        Authentication auth = new UsernamePasswordAuthenticationToken(user, "rest", gaList);
        ctx.setAuthentication(auth);
    }

    /**
     * Returns all mushroom entities in JSON
     *
     * @return
     */
    @RequestMapping("/")
    public List<MushroomDTO> getAll(HttpServletRequest request) {
        List<MushroomDTO> list = null;
        try {
            login();
            list = mushroomService.getAllMushrooms();
        } finally {
            SecurityContextHolder.clearContext();
        }

        return list;
    }

    /**
     * returns all mushroom of type
     *
     * @param mushroomType
     * @return all mushroom of type
     */
    @RequestMapping("/getByType")
    public List<MushroomDTO> getByType(@RequestParam(required = true, value = "mushroomType") MushroomTypeDTO mushroomType) {
        List<MushroomDTO> list = null;
        try {
            login();
            switch (mushroomType) {
                case EDIBLE:
                    list = mushroomService.getEdibleMushrooms();
                    break;
                case POISONOUS:
                    list = mushroomService.getPoisonousMushrooms();
                    break;
                case UNEDIBLE:
                    list = mushroomService.getUnedibleMushrooms();
                    break;
                default:
                    list = null;
            }
        } finally {
            SecurityContextHolder.clearContext();
        }

        return list;

    }

    /**
     *
     * @param id
     * @return mushroom by id
     */
    @RequestMapping("/detail")
    public MushroomDTO mushroomHunterDetail(
            @RequestParam(required = true, value = "id") long id) {
        MushroomDTO mushroom = null;
        try {
            login();
            mushroom = mushroomService.getMushroomById(id);
        } finally {
            SecurityContextHolder.clearContext();
        }

        return mushroom;
    }

    /**
     * add mushroom
     *
     * @param mushroom
     * @return string result of operation
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@Validated @RequestBody final MushroomDTO mushroom, BindingResult bindingResult) {
        try {
            login();
            if (bindingResult.hasErrors()) {
                logger.info("Validation errors on update mushroom: {}", mushroom);
                return "error";
            }

            try {
                mushroomService.createMushroom(mushroom);
            } catch (DataAccessException e) {
                logger.error("Failed to create mushroom: {}, {}", mushroom, e);
                return "error";
            }
        } finally {
            SecurityContextHolder.clearContext();
        }
        return "ok";
    }

    /**
     * edit mushroom
     *
     * @param mushroom
     * @return string result of operation
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String edit(@Validated @RequestBody final MushroomDTO mushroom, BindingResult bindingResult) {
        try {
            login();
            if (bindingResult.hasErrors()) {
                logger.info("Validation errors on update mushroom: {}", mushroom);
                return "error";
            }

            try {
                mushroomService.updateMushroom(mushroom);
            } catch (DataAccessException e) {
                logger.error("Failed to update mushroom: {}, {}", mushroom, e);
                return "error";
            }
        } finally {
            SecurityContextHolder.clearContext();
        }
        return "ok";
    }

    /**
     * delete mushroom by id
     *
     * @param id
     * @return string result of operation
     */
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public String delete(@RequestParam(required = true, value = "id") long id) {
        try {
            login();
            MushroomDTO mushroom;
            try {
                mushroom = mushroomService.getMushroomById(id);
            } catch (DataAccessException e) {
                logger.error("Failed to obtain mushroom with id: {}, {}", id, e);
                return String.format("error: failed to obtain mushroom with id: {}", id);
            }
            try {
                mushroomService.deleteMushroom(mushroom);
            } catch (DataAccessException e) {
                logger.error("Delete: Failed to delete mushroom with id: {}, {}", id, e);
                return String.format("error: Failed to delete mushroom with id: {}", id);
            }
        } finally {
            SecurityContextHolder.clearContext();
        }
        return "ok";
    }
}
