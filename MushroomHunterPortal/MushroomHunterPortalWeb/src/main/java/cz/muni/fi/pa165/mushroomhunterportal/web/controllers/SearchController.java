package cz.muni.fi.pa165.mushroomhunterportal.web.controllers;

import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.ForestService;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomHunterService;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.ForestDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Jan Bruzl
 */
@Controller
public class SearchController {
    @Autowired
    private MushroomHunterService mushroomHunterService;

    @Autowired
    private ForestService forestService;       
    
    @Autowired
    private MushroomService mushroomService;

    
    @RequestMapping("/search")
    public void search(@RequestParam(required = true, value = "search")String search, Model model){
        //mushrooms
        List<MushroomDTO> mushroomList = mushroomService.findMushroom(search);
        model.addAttribute("allMushrooms", mushroomList);
        
        //forests
        List<ForestDTO> forestList = forestService.findForest(search);
        model.addAttribute("allForests", forestList);
        
        //hunters
        List<MushroomHunterDTO> hunterList = mushroomHunterService.findHunter(search);
        model.addAttribute("allHunters", hunterList);
    }
}
