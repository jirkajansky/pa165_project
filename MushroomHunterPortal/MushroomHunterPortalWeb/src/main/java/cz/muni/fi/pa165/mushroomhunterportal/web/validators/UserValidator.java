package cz.muni.fi.pa165.mushroomhunterportal.web.validators;

import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.UserDTO;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * validator for user controller
 *
 * @author Jiří Jánský
 */
public class UserValidator implements Validator {

    @Autowired
    MushroomHunterValidator mushroomHunterValidator;

    /**
     * return true for supports class
     *
     * @param paramClass
     * @return
     */
    @Override
    public boolean supports(Class<?> paramClass) {
        return UserDTO.class.equals(paramClass);
    }

    /**
     * validation for hunter
     *
     * @param obj
     * @param errors
     */
    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "id.required");

        UserDTO user = (UserDTO) obj;
        if (user.getId() < 0) {
            errors.rejectValue("id", "negativeValue", new Object[]{"'id'"}, "id can't be negative");
        }

        if (user.getHunter()!=null)
        {
            errors.pushNestedPath("hunter");
            ValidationUtils.invokeValidator(mushroomHunterValidator, user.getHunter(), errors);
            errors.popNestedPath();
        }

        if (user.getMail() == null || user.getMail().isEmpty()) {
            errors.rejectValue("mail", "mail.required");
        } else {

            Pattern r = Pattern.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", Pattern.CASE_INSENSITIVE);

            Matcher m = r.matcher(user.getMail());
            if (!m.find()) {
                errors.rejectValue("mail", "mail.badFormat");
            }
        }
    }
}
