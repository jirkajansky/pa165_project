package cz.muni.fi.pa165.mushroomhunterportal.web.rest.controllers;

import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomHunterService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.RoleDTO;
import cz.muni.fi.pa165.mushroomhunterportal.web.controllers.MushroomController;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Jan Trnka
 */
@RequestMapping("/rest/hunters")
@RestController
public class MushroomHunterRestController {

    private final Logger logger = LoggerFactory.getLogger(MushroomController.class);

    @Autowired
    MushroomHunterService mushroomHunterService;

    @RequestMapping("/test")
    public String test() {
        return "Hello world!";
    }
    @Autowired
    @Qualifier("mushroomHunterValidator")
    private Validator validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    /**
     * Set security context for REST controllers for Service layer
     * authentication & authorization.
     */
    private void login() {
        SecurityContext ctx = SecurityContextHolder.createEmptyContext();
        SecurityContextHolder.setContext(ctx);
        List<GrantedAuthority> gaList = new ArrayList<>();
        gaList.add(new SimpleGrantedAuthority(RoleDTO.ROLE_ADMINISTRATOR.toString()));
        User user = new User("rest", "rest",
                true, true, true, true, gaList);
        Authentication auth = new UsernamePasswordAuthenticationToken(user, "rest", gaList);
        ctx.setAuthentication(auth);
    }

    /**
     * Returns all mushroomHunter entities in JSON format
     *
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<MushroomHunterDTO> getAll() {
        List<MushroomHunterDTO> list = null;
        try {
            login();
            list = mushroomHunterService.getAllHunters();
        } finally {
            SecurityContextHolder.clearContext();
        }
        return list;
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public MushroomHunterDTO mushroomHunterDetail(
            @RequestParam(required = true, value = "id") long id) {
        MushroomHunterDTO hunter = null;
        try {
            login();

            try {
                hunter = mushroomHunterService.getById(id);
            } catch (DataAccessException e) {
                logger.error("Failed to obtain hunter with id: {}, {}", id, e);
                hunter = null;
            }
        } finally {
            SecurityContextHolder.clearContext();
        }
        return hunter;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@Validated @RequestBody final MushroomHunterDTO hunter, BindingResult bindingResult) {
        try {
            login();
            if (bindingResult.hasErrors()) {
                logger.info("Validation errors on edit hunter: {}", hunter);
                return "error";
            }

            try {
                mushroomHunterService.createHunter(hunter);
            } catch (DataAccessException e) {
                logger.error("Failed to create hunter: {}, {}", hunter, e);
                return "error";
            }
        } finally {
            SecurityContextHolder.clearContext();
        }
        return "ok";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String edit(@Validated @RequestBody final MushroomHunterDTO hunter, BindingResult bindingResult) {
        try {
            login();
            if (bindingResult.hasErrors()) {
                logger.info("Validation errors on edit hunter: {}", hunter);
                return "error";
            }

            try {
                mushroomHunterService.updateHunter(hunter);
            } catch (DataAccessException e) {
                logger.error("Failed to update hunter: {}, {}", hunter, e);
                return "error";
            }
        } finally {
            SecurityContextHolder.clearContext();
        }
        return "ok";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public String delete(@RequestParam(required = true, value = "id") long id) {
        try {
            login();
            MushroomHunterDTO hunter;
            try {
                hunter = mushroomHunterService.getById(id);
            } catch (DataAccessException e) {
                logger.error("Failed to obtain hunter with id: {}, {}", id, e);
                return String.format("error: failed to obtain hunter with id: {}", id);
            }
            try {
                mushroomHunterService.deleteHunter(hunter);
            } catch (DataAccessException e) {
                logger.error("Delete: Failed to delete mushroom with id: {}, {}", id, e);
                return String.format("error: Failed to delete mushroom with id: {}", id);
            }
        } finally {
            SecurityContextHolder.clearContext();
        }
        return "ok";
    }

}
