package cz.muni.fi.pa165.mushroomhunterportal.web;

import cz.muni.fi.pa165.mushroomhunterportal.web.validators.ForestValidator;
import cz.muni.fi.pa165.mushroomhunterportal.web.validators.MushroomValidator;
import cz.muni.fi.pa165.mushroomhunterportal.web.validators.MushroomHunterValidator;
import cz.muni.fi.pa165.mushroomhunterportal.web.validators.UserValidator;
import cz.muni.fi.pa165.mushroomhunterportal.web.validators.VisitValidator;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

/**
 *
 * @author Jan Bruzl <bruzl@progenium.cz>
 */
@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = "cz.muni.fi.pa165.mushroomhunterportal.dao.entities")
@ComponentScan(basePackages = "cz.muni.fi.pa165.mushroomhunterportal")
@EnableJpaRepositories("cz.muni.fi.pa165.mushroomhunterportal.dao")
public class MushroomHunterPortalWeb extends WebMvcConfigurerAdapter {
    
    @Bean
    Mapper mapper() {
        return new DozerBeanMapper();
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(MushroomHunterPortalWeb.class);
    }

    @Bean
    public TemplateResolver templateResolver() {
        TemplateResolver tempRes = new SpringResourceTemplateResolver();

        tempRes.setCharacterEncoding("UTF-8");
        tempRes.setTemplateMode("HTML5");
        tempRes.setSuffix(".html");
        tempRes.setPrefix("classpath:/templates/");
        tempRes.setCacheable(false);
        return tempRes;
    }

    @Bean
    public SpringSecurityDialect thymeleafSecurityDialect(){
        return new SpringSecurityDialect();
    } 

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine ste = new SpringTemplateEngine();
        ste.setTemplateResolver(templateResolver());
        ste.setMessageSource(messageSource());
        ste.addDialect(thymeleafSecurityDialect());
        return ste;
    }

    @Bean
    public ViewResolver viewResolver() {
        ThymeleafViewResolver tvr = new ThymeleafViewResolver();

        tvr.setCharacterEncoding("UTF-8");
        tvr.setTemplateEngine(templateEngine());

        return tvr;
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    CharacterEncodingFilter characterEncodingFilter() {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        return filter;
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource ms = new ReloadableResourceBundleMessageSource();

        ms.setBasename("classpath:/I18L/messages");
        ms.setDefaultEncoding("UTF-8");
        ms.setCacheSeconds(0);

        return ms;
    }

    @Bean
    public MushroomValidator mushroomValidator() {
        return new MushroomValidator();
    }

    @Bean
    ForestValidator forestValidator() {
        return new ForestValidator();
    }

    @Bean
    MushroomHunterValidator mushroomHunterValidator() {
        return new MushroomHunterValidator();
    }

    @Bean
    VisitValidator visitValidator() {
        return new VisitValidator();
    }
    
    @Bean
    UserValidator userValidator(){
        return new UserValidator();
    }            

}
