package cz.muni.fi.pa165.mushroomhunterportal.web.validators;

import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomDTO;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import org.springframework.validation.Validator;
/**
 * validator for mushroom controller
 * @author Jiri Jansky
 *
 */

public class MushroomValidator implements Validator {

    /**
     * return true for supports class
     * @param paramClass
     * @return 
     */
    @Override
    public boolean supports(Class<?> paramClass) {
        return MushroomDTO.class.equals(paramClass);
    } 
    /**
     * validation for mushroom
     * @param obj
     * @param errors 
     */
    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "id.required");
         
        MushroomDTO mush = (MushroomDTO) obj;
        if(mush.getId() < 0){
            errors.rejectValue("id", "negativeValue", new Object[]{"'id'"}, "id can't be negative");
        }
         
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "type.required");
    }
}
