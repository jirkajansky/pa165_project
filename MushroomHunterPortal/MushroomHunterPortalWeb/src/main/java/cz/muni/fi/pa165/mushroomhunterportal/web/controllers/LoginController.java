package cz.muni.fi.pa165.mushroomhunterportal.web.controllers;

import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.MushroomHunterDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.RoleDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.UserDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.VisitDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.MushroomHunterService;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.UserService;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.VisitService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Jan Trnka
 */
@Controller
public class LoginController {

    private final Logger logger = LoggerFactory.getLogger(MushroomController.class);

    @Autowired
    UserService userService;

    @Autowired
    VisitService visitService;

    @Autowired
    private MushroomHunterService mushroomHunterService;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    @Qualifier("userValidator")
    private Validator userValidator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(userValidator);
    }

    @ModelAttribute("userDTO")
    public UserDTO getNewMushroom() {
        UserDTO u = new UserDTO();
        MushroomHunterDTO m = new MushroomHunterDTO();
        u.setHunter(m);
        return u;
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/signup")
    public String signup() {
        return "signup";
    }

    @RequestMapping(value = "/signup-submit", params = "save", method = RequestMethod.POST)
    public String userSignupSubmit(
            @Validated final UserDTO user,
            BindingResult bindingResult,
            @RequestParam(value = "password", required = true) String password,
            Model model
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("userDTO", user);
            return "/signup";
        } else if (userService.checkMailIsAlreadyTaken(user.getMail())) {
            model.addAttribute("mailIsAlreadyTaken", true);
            return "/signup";
        } else if (password.isEmpty()) {
            model.addAttribute("emptyPassword", true);
            return "/signup";
        } else if (password.length() < 6) {
            model.addAttribute("shortPassword", true);
        }
        try {
            mushroomHunterService.createHunter(user.getHunter());
            user.setRole(RoleDTO.ROLE_USER);
            userService.createUser(user, encoder.encode(password));

        } catch (DataAccessException e) {
            logger.error("Failed to create user: {}, {}", user, e);
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/profile")
    public String myProfile(Model model) {
        model.addAttribute("userDTO", userService.getLogedUser());
        List<VisitDTO> visits = visitService.getVisitsByHunter(userService.getLogedUser().getHunter());
        model.addAttribute("visits", visits);

        return "profile";
    }

    @RequestMapping(value = "/profile/edit")
    public String myProfileEdit(Model model) {
        model.addAttribute("userDTO", userService.getLogedUser());
        return "profile/edit";
    }

    @RequestMapping(value = "/profile/edit-submit")
    public String myProfileEditSubmit(@Validated final UserDTO user,
            BindingResult bindingResult,
            Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("userDTO", user);
            return "/profile/edit";
        }

        try {
            mushroomHunterService.updateHunter(user.getHunter());

        } catch (DataAccessException e) {
            logger.error("Failed to update user: {}, {}", user, e);
        }

        return "redirect:/profile";
    }
}
