package cz.muni.fi.pa165.mushroomhunterportal.web.controllers;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.ForestService;
import cz.muni.fi.pa165.mushroomhunterportal.api.interfaces.VisitService;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.ForestDTO;
import cz.muni.fi.pa165.mushroomhunterportal.api.DTO.VisitDTO;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author Jan Bruzl
 */
@Controller
public class ForestController {
    
    private final Logger logger = LoggerFactory.getLogger(ForestController.class);

    @Autowired
    private ForestService forestService;
    
    @Autowired
    private VisitService visitService;
    
    @Autowired
    @Qualifier("forestValidator")
    private Validator validator;
 
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
    
    @ModelAttribute("forestDTO")
    public ForestDTO getNewForest(){
        return new ForestDTO();
    }

    @RequestMapping("/forests")
    public void forests() {
    }
    
    @RequestMapping("/forests/detail")
    public void  forestDetail(@RequestParam(required = true, value = "id") long id, Model model){
        model.addAttribute("id", id);
        
        ForestDTO forest;
        try {
            forest = forestService.getForestById(id);
        } catch (DataAccessException ex) {
            logger.error("Failed to obtain forest with id: {}, {}", id, ex);
            forest = null;
        }

        model.addAttribute("forestDTO", forest);
        List<VisitDTO> visits = visitService.getVisitsByForest(forest);
        model.addAttribute("visitDTO",visits);

    }

    @RequestMapping("/forests/add")
    public void forestAdd() {

    }
    
    @RequestMapping("/forests/edit")
    public void forestEdit(@RequestParam(required = true, value = "id") long id, Model model) {
        model.addAttribute("id", id);

        ForestDTO forest;
        try {
            forest = forestService.getForestById(id);
        } catch (DataAccessException ex) {
            logger.error("Failed to obtain forest with id: {}, {}", id, ex);
            forest = null;
        }

        model.addAttribute("forestDTO", forest);
    }
    
    @RequestMapping(value="/forests/add-submit", params = {"save"}, method = RequestMethod.POST)
    public String forestAddSubmit(@Valid final ForestDTO forest, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            logger.info("Returning error page");   
            model.addAttribute("forestDTO", forest);
            return "/forests/add";
        }
        try{
            
            forestService.createForest(forest);
        } catch (DataAccessException ex) {
            logger.error("Failed to create forest : {}, {}", forest, ex);
        }
        return "redirect:/forests";
    }
    
    @RequestMapping(value="/forests/edit-submit", params = {"save"}, method = RequestMethod.POST)
    public String forestEditSubmit(@Validated final ForestDTO forest, BindingResult bindingResult,Model model) {
        if (bindingResult.hasErrors()) {
            logger.info("Returning error page");
            model.addAttribute("forestDTO", forest);
            return "/forests/edit";
        }
        try{
            forestService.updateForest(forest);
        } catch (DataAccessException ex) {
            logger.error("Failed to update forest : {}, {}", forest, ex);
        }
        return "redirect:/forests";
    }

    @RequestMapping("/forests/delete")
    public String forestDelete(@RequestParam(required = true, value = "id") long id) {
        ForestDTO forest;
        try {
            forest = forestService.getForestById(id);
        } catch (DataAccessException ex) {
            logger.error("Delete: Failed to obtain forest with id: {}, {}", id, ex);
            return "redirect:/forests";
        }

        try {
            forestService.deleteForest(forest);
        } catch (DataAccessException ex) {
            logger.error("Delete: Failed to delete forest with id: {}, {}", id, ex);
        }

        return "redirect:/forests";
    }
    
    @ModelAttribute("allForests")
    public List<ForestDTO> getAllForests() {
        return forestService.getAllForests();
    }
    
}
